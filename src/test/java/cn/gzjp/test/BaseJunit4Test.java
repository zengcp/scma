package cn.gzjp.test;


import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 * 单元测试继承我
 * @author jp_zh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)//表示整合JUnit4进行测试
@ContextConfiguration(locations={"classpath:spring/spring-context.xml"})//加载spring配置文件
public class BaseJunit4Test {

}
