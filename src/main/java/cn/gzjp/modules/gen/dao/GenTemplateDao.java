/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package cn.gzjp.modules.gen.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.gen.entity.GenTemplate;

/**
 * 代码模板DAO接口
 * @author ThinkGem
 * @version 2013-10-15
 */
@MyBatisDao
public interface GenTemplateDao extends CrudDao<GenTemplate> {
	
}
