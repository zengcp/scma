/**
 * Copyright &copy; 2015-2020 <a href="http://www..org/"></a> All rights reserved.
 */
package cn.gzjp.modules.sys.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.shiro.session.Session;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.security.Digests;
import cn.gzjp.common.security.shiro.session.SessionDAO;
import cn.gzjp.common.service.BaseService;
import cn.gzjp.common.service.ServiceException;
import cn.gzjp.common.utils.CacheUtils;
import cn.gzjp.common.utils.Encodes;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.modules.sys.dao.MenuDao;
import cn.gzjp.modules.sys.dao.RoleDao;
import cn.gzjp.modules.sys.dao.UserDao;
import cn.gzjp.modules.sys.entity.Menu;
import cn.gzjp.modules.sys.entity.Office;
import cn.gzjp.modules.sys.entity.Role;
import cn.gzjp.modules.sys.entity.User;
import cn.gzjp.modules.sys.utils.LogUtils;
import cn.gzjp.modules.sys.utils.UserUtils;

/**
 * 系统管理，安全相关实体的管理类,包括用户、角色、菜单.
 * @author 
 * @version 2013-12-05
 */
@Service
@Transactional(readOnly = true)
public class SystemService extends BaseService implements InitializingBean {
	
	public static final String HASH_ALGORITHM = "SHA-1";
	public static final int HASH_INTERATIONS = 1024;
	public static final int SALT_SIZE = 8;
	
	@Autowired
	private UserDao userDao;
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private MenuDao menuDao;
	@Autowired
	private SessionDAO sessionDao;
	
	public SessionDAO getSessionDao() {
		return sessionDao;
	}


	//-- User Service --//
	
	/**
	 * 获取用户
	 * @param id
	 * @return
	 */
	public User getUser(String id) {
		return UserUtils.get(id);
	}

	/**
	 * 根据登录名获取用户
	 * @param loginName
	 * @return
	 */
	public User getUserByLoginName(String loginName) {
		return UserUtils.getByLoginName(loginName);
	}
	
	public Page<User> findUser(Page<User> page, User user) {
		// 生成数据权限过滤条件（dsf为dataScopeFilter的简写，在xml中使用 ${sqlMap.dsf}调用权限SQL）
		user.getSqlMap().put("dsf", dataScopeFilter(user.getCurrentUser(), "o", "a"));
		// 设置分页参数
		user.setPage(page);
		// 执行分页查询
		page.setList(userDao.findList(user));
		return page;
	}
	
	/**
	 * 无分页查询人员列表
	 * @param user
	 * @return
	 */
	public List<User> findUser(User user){
		// 生成数据权限过滤条件（dsf为dataScopeFilter的简写，在xml中使用 ${sqlMap.dsf}调用权限SQL）
		user.getSqlMap().put("dsf", dataScopeFilter(user.getCurrentUser(), "o", "a"));
		List<User> list = userDao.findList(user);
		return list;
	}

	/**
	 * 通过部门ID获取用户列表，仅返回用户id和name（树查询用户时用）
	 * @param user
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<User> findUserByOfficeId(String officeId) {
		List<User> list = (List<User>)CacheUtils.get(UserUtils.USER_CACHE, UserUtils.USER_CACHE_LIST_BY_OFFICE_ID_ + officeId);
		if (list == null){
			User user = new User();
			user.setOffice(new Office(officeId));
			list = userDao.findUserByOfficeId(user);
			CacheUtils.put(UserUtils.USER_CACHE, UserUtils.USER_CACHE_LIST_BY_OFFICE_ID_ + officeId, list);
		}
		return list;
	}
	
	@Transactional(readOnly = false)
	public void saveUser(User user) {
		if (StringUtils.isBlank(user.getId())){
			user.preInsert();
			userDao.insert(user);
		}else{
			// 清除原用户机构用户缓存
			User oldUser = userDao.get(user.getId());
			if (oldUser.getOffice() != null && oldUser.getOffice().getId() != null){
				CacheUtils.remove(UserUtils.USER_CACHE, UserUtils.USER_CACHE_LIST_BY_OFFICE_ID_ + oldUser.getOffice().getId());
			}
			// 更新用户数据
			user.preUpdate();
			userDao.update(user);
		}
		if (StringUtils.isNotBlank(user.getId())){
			// 更新用户与角色关联
			userDao.deleteUserRole(user);
			if (user.getRoleList() != null && user.getRoleList().size() > 0){
				userDao.insertUserRole(user);
			}else{
				throw new ServiceException(user.getLoginName() + "没有设置角色！");
			}
			// 清除用户缓存
			UserUtils.clearCache(user);
//			// 清除权限缓存
//			systemRealm.clearAllCachedAuthorizationInfo();
		}
	}
	
	@Transactional(readOnly = false)
	public void updateUserInfo(User user) {
		user.preUpdate();
		userDao.updateUserInfo(user);
		// 清除用户缓存
		UserUtils.clearCache(user);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}
	
	@Transactional(readOnly = false)
	public void deleteUser(User user) {
		userDao.delete(user);
		// 清除用户缓存
		UserUtils.clearCache(user);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}
	
	@Transactional(readOnly = false)
	public void updatePasswordById(String id, String loginName, String newPassword) {
		User user = new User(id);
		user.setPassword(entryptPassword(newPassword));
		userDao.updatePasswordById(user);
		// 清除用户缓存
		user.setLoginName(loginName);
		UserUtils.clearCache(user);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}
	
	@Transactional(readOnly = false)
	public void updateUserLoginInfo(User user) {
		// 保存上次登录信息
		user.setOldLoginIp(user.getLoginIp());
		user.setOldLoginDate(user.getLoginDate());
		// 更新本次登录信息
		user.setLoginIp(UserUtils.getSession().getHost());
		user.setLoginDate(new Date());
		userDao.updateLoginInfo(user);
	}
	
	/**
	 * 生成安全的密码，生成随机的16位salt并经过1024次 sha-1 hash
	 */
	public static String entryptPassword(String plainPassword) {
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		byte[] hashPassword = Digests.sha1(plainPassword.getBytes(), salt, HASH_INTERATIONS);
		return Encodes.encodeHex(salt)+Encodes.encodeHex(hashPassword);
	}
	
	/**
	 * 验证密码
	 * @param plainPassword 明文密码
	 * @param password 密文密码
	 * @return 验证成功返回true
	 */
	public static boolean validatePassword(String plainPassword, String password) {
		byte[] salt = Encodes.decodeHex(password.substring(0,16));
		byte[] hashPassword = Digests.sha1(plainPassword.getBytes(), salt, HASH_INTERATIONS);
		return password.equals(Encodes.encodeHex(salt)+Encodes.encodeHex(hashPassword));
	}
	
	/**
	 * 获得活动会话
	 * @return
	 */
	public Collection<Session> getActiveSessions(){
		return sessionDao.getActiveSessions(false);
	}
	
	//-- Role Service --//
	
	public Role getRole(String id) {
		return roleDao.get(id);
	}

	public Role getRoleByName(String name) {
		Role r = new Role();
		r.setName(name);
		return roleDao.getByName(r);
	}
	
	public Role getRoleByEnname(String enname) {
		Role r = new Role();
		r.setEnname(enname);
		return roleDao.getByEnname(r);
	}
	
	public List<Role> findRole(Role role){
		return roleDao.findList(role);
	}
	
	public List<Role> findAllRole(){
		return UserUtils.getRoleList();
	}
	
	@Transactional(readOnly = false)
	public void saveRole(Role role) {
		if (StringUtils.isBlank(role.getId())){
			role.preInsert();
			roleDao.insert(role);
		}else{
			role.preUpdate();
			roleDao.update(role);
		}
		// 更新角色与菜单关联
		roleDao.deleteRoleMenu(role);
		if (role.getMenuList().size() > 0){
			roleDao.insertRoleMenu(role);
		}
		// 更新角色与部门关联
		roleDao.deleteRoleOffice(role);
		if (role.getOfficeList().size() > 0){
			roleDao.insertRoleOffice(role);
		}
		// 清除用户角色缓存
		UserUtils.removeCache(UserUtils.CACHE_ROLE_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}

	@Transactional(readOnly = false)
	public void deleteRole(Role role) {
		roleDao.delete(role);
		// 清除用户角色缓存
		UserUtils.removeCache(UserUtils.CACHE_ROLE_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}
	
	@Transactional(readOnly = false)
	public Boolean outUserInRole(Role role, User user) {
		List<Role> roles = user.getRoleList();
		for (Role e : roles){
			if (e.getId().equals(role.getId())){
				roles.remove(e);
				saveUser(user);
				return true;
			}
		}
		return false;
	}
	
	@Transactional(readOnly = false)
	public User assignUserToRole(Role role, User user) {
		if (user == null){
			return null;
		}
		List<String> roleIds = user.getRoleIdList();
		if (roleIds.contains(role.getId())) {
			return null;
		}
		user.getRoleList().add(role);
		saveUser(user);
		return user;
	}

	//-- Menu Service --//
	
	public Menu getMenu(String id) {
		return menuDao.get(id);
	}

	public List<Menu> findAllMenu(){
		return UserUtils.getMenuList();
	}
	
	@Transactional(readOnly = false)
	public void saveMenu(Menu menu) {
		
		// 获取父节点实体
		menu.setParent(this.getMenu(menu.getParent().getId()));
		
		// 获取修改前的parentIds，用于更新子节点的parentIds
		String oldParentIds = menu.getParentIds(); 
		
		// 设置新的父节点串
		menu.setParentIds(menu.getParent().getParentIds()+menu.getParent().getId()+",");

		// 保存或更新实体
		if (StringUtils.isBlank(menu.getId())){
			boolean isCreateCrud = StringUtils.isNoneBlank(menu.getIsCrud())&&"1".equals(menu.getIsCrud())&&StringUtils.isNoneBlank(menu.getHref());
			if(isCreateCrud){//自动创建增删改查 子菜单按钮
				String permission = menu.getHref().replaceAll("/", ":");
				if(permission.startsWith(":")){
					permission =permission.substring(1, permission.length());
				}
				String listPermission = permission+"list";
				menu.setPermission(listPermission);//列表权限
			}
			menu.preInsert();
			menuDao.insert(menu);
			if(isCreateCrud){
				this.createCrudMenu(menu);
			}
		}else{
			menu.preUpdate();
			menuDao.update(menu);
			// 更新子节点 parentIds
			Menu m = new Menu();
			m.setParentIds("%,"+menu.getId()+",%");
			List<Menu> list = menuDao.findByParentIdsLike(m);
			for (Menu e : list){
				e.setParentIds(e.getParentIds().replace(oldParentIds, menu.getParentIds()));
				menuDao.updateParentIds(e);
			}
		}
		// 清除用户菜单缓存
		UserUtils.removeCache(UserUtils.CACHE_MENU_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
		// 清除日志相关缓存
		CacheUtils.remove(LogUtils.CACHE_MENU_NAME_PATH_MAP);
	}
	
	
	
	@Transactional(readOnly = false)
	private void createCrudMenu(Menu menu) {
		Menu addMenu = new Menu();
		addMenu.setName("新增");
		// 获取父节点实体
		addMenu.setParent(menu);
		// 设置新的父节点串
		addMenu.setParentIds(menu.getParentIds()+menu.getId()+",");
		addMenu.setSort(10);
		addMenu.setIsShow("0");//不展示
		addMenu.setPermission(menu.getPermission().replaceAll("list", "add"));
		addMenu.preInsert();
		menuDao.insert(addMenu);
		
		Menu delMenu = new Menu();
		delMenu.setName("删除");
		// 获取父节点实体
		delMenu.setParent(menu);
		// 设置新的父节点串
		delMenu.setParentIds(menu.getParentIds()+menu.getId()+",");
		delMenu.setSort(20);
		delMenu.setIsShow("0");//不展示
		delMenu.setPermission(menu.getPermission().replaceAll("list", "del"));
		delMenu.preInsert();
		menuDao.insert(delMenu);
		
		Menu editMenu = new Menu();
		editMenu.setName("修改");
		// 获取父节点实体
		editMenu.setParent(menu);
		// 设置新的父节点串
		editMenu.setParentIds(menu.getParentIds()+menu.getId()+",");
		editMenu.setSort(30);
		editMenu.setIsShow("0");//不展示
		editMenu.setPermission(menu.getPermission().replaceAll("list", "edit"));
		editMenu.preInsert();
		menuDao.insert(editMenu);
		
		
		Menu viewMenu = new Menu();
		viewMenu.setName("查看");
		// 获取父节点实体
		viewMenu.setParent(menu);
		// 设置新的父节点串
		viewMenu.setParentIds(menu.getParentIds()+menu.getId()+",");
		viewMenu.setSort(40);
		viewMenu.setIsShow("0");//不展示
		viewMenu.setPermission(menu.getPermission().replaceAll("list", "view"));
		viewMenu.preInsert();
		menuDao.insert(viewMenu);
		
		Menu importMenu = new Menu();
		importMenu.setName("导入");
		// 获取父节点实体
		importMenu.setParent(menu);
		// 设置新的父节点串
		importMenu.setParentIds(menu.getParentIds()+menu.getId()+",");
		importMenu.setSort(50);
		importMenu.setIsShow("0");//不展示
		importMenu.setPermission(menu.getPermission().replaceAll("list", "import"));
		importMenu.preInsert();
		menuDao.insert(importMenu);
		
		Menu exportMenu = new Menu();
		exportMenu.setName("导出");
		// 获取父节点实体
		exportMenu.setParent(menu);
		// 设置新的父节点串
		exportMenu.setParentIds(menu.getParentIds()+menu.getId()+",");
		exportMenu.setSort(60);
		exportMenu.setIsShow("0");//不展示
		exportMenu.setPermission(menu.getPermission().replaceAll("list", "export"));
		exportMenu.preInsert();
		menuDao.insert(exportMenu);
	}


	@Transactional(readOnly = false)
	public void updateMenuSort(Menu menu) {
		menuDao.updateSort(menu);
		// 清除用户菜单缓存
		UserUtils.removeCache(UserUtils.CACHE_MENU_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
		// 清除日志相关缓存
		CacheUtils.remove(LogUtils.CACHE_MENU_NAME_PATH_MAP);
	}

	@Transactional(readOnly = false)
	public void deleteMenu(Menu menu) {
		menuDao.delete(menu);
		// 清除用户菜单缓存
		UserUtils.removeCache(UserUtils.CACHE_MENU_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
		// 清除日志相关缓存
		CacheUtils.remove(LogUtils.CACHE_MENU_NAME_PATH_MAP);
	}
	
	/**
	 * 获取Key加载信息
	 */
	public static boolean printKeyLoadMessage(){
		return true;
	}


	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}
	

	
}
