/**
 * Copyright &copy; 2015-2020 <a href="http://www..org/"></a> All rights reserved.
 */
package cn.gzjp.modules.sys.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.service.CrudService;
import cn.gzjp.common.utils.CacheUtils;
import cn.gzjp.modules.sys.dao.DictDao;
import cn.gzjp.modules.sys.entity.Dict;
import cn.gzjp.modules.sys.utils.DictUtils;

/**
 * 字典Service
 * @author 
 * @version 2014-05-16
 */
@Service
@Transactional(readOnly = true)
public class DictService extends CrudService<DictDao, Dict> {
	
	/**
	 * 查询字段类型列表
	 * @return
	 */
	public List<String> findTypeList(){
		return dao.findTypeList(new Dict());
	}

	@Override
	@Transactional(readOnly = false)
	public void save(Dict dict) {
		super.save(dict);
		CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(Dict dict) {
		super.delete(dict);
		CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
	}

}
