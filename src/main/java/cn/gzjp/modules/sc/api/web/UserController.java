package cn.gzjp.modules.sc.api.web;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import cn.gzjp.common.config.Global;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.sc.api.bean.CommonJson;
import cn.gzjp.modules.sc.api.bean.PageJson;
import cn.gzjp.modules.sc.entity.Background;
import cn.gzjp.modules.sc.entity.Dynamic;
import cn.gzjp.modules.sc.entity.Member;
import cn.gzjp.modules.sc.entity.MemberFollow;
import cn.gzjp.modules.sc.service.*;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.exception.WxErrorException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * 微信小程序用户接口
 *
 */
@Controller("frontUser")
@Api(value = "用户接口", description = "用户接口")
@RequestMapping("${frontPath}/user")
public class UserController extends BaseController {

    WxMaService wxService = WxService.getWxMaService();
    @Autowired
    private MemberService memberService;
	@Autowired
    private MemberFollowService memberFollowService;
	@Autowired
	private DynamicService dynamicService;



	/**
	 * 用户登录
	 */
	@ApiOperation(value = "用户登录")
	@RequestMapping(value = "login",method = RequestMethod.GET)
	@ApiImplicitParams ({@ApiImplicitParam(name="code",value="code",dataType="String",paramType="query",required =true)
	})
	@ResponseBody
	public String login(String code) {
		CommonJson result = new CommonJson();
		log.info("用户微信登录 code="+code);
		if (StringUtils.isBlank(code)) {
			result.setMsg("code不能为空");
			result.setCode(1);
			return this.mapper.toJson(result);
		}
		try {
			WxMaJscode2SessionResult session = this.wxService.getUserService().getSessionInfo(code);
			String openid = session.getOpenid();
			log.info("openid="+openid+"|sessionKey="+session.getSessionKey());
			if(StringUtils.isNotBlank(openid)){
				Member member = memberService.get(openid);
				if(member==null){
					member = new Member();
					member.setId(openid);
					member.setFansCount(0);
					member.setFollowCount(0);
					member.setDynamicCount(0);
					memberService.save(member);//新增用户
				}
			}
			result.setData(session);
			log.info("返回微信小程序"+mapper.toJson(result));
			return this.mapper.toJson(result);
		} catch (WxErrorException e) {
			log.info("异常信息="+e.getMessage());
			return e.toString();
		}
	}


	/**
	 * 用户信息获取
	 * @param sessionKey
	 * @param encryptedData
	 * @param iv
	 * @return
	 */
	@ApiOperation(value = "我的个人信息(用户登录后调用该接口，后台获取用户头像昵称)")
	@RequestMapping(value = "myinfo",method = RequestMethod.POST)
	@ApiImplicitParams ({@ApiImplicitParam(name="sessionKey",value="会话密钥",dataType="String",paramType="form",required =true),
			@ApiImplicitParam(name="encryptedData",value="消息密文",dataType="String",paramType="form",required =true),
			@ApiImplicitParam(name="iv",value="加密算法的初始向量",dataType="String",paramType="form",required =true)
	})
	@ResponseBody
	public String myinfo(String sessionKey, String encryptedData, String iv) {
		CommonJson result = new CommonJson();
		log.info("获取用户信息 sessionKey="+sessionKey+"|encryptedData="+encryptedData+"|iv="+iv);
		if (StringUtils.isBlank(sessionKey)) {
			result.setMsg("参数sessionKey不能为空");
			result.setCode(1);
			log.info("返回给小程序数据:" + this.mapper.toJson(result));
			return this.mapper.toJson(result);
		}
		if (StringUtils.isBlank(encryptedData)) {
			result.setMsg("参数encryptedData不能为空");
			result.setCode(1);
			log.info("返回给小程序数据:" + this.mapper.toJson(result));
			return this.mapper.toJson(result);
		}
		if (StringUtils.isBlank(iv)) {
			result.setMsg("参数ive不能为空");
			result.setCode(1);
			log.info("返回给小程序数据:" + this.mapper.toJson(result));
			return this.mapper.toJson(result);
		}
		// 解密用户信息
		WxMaUserInfo userInfo = this.wxService.getUserService().getUserInfo(sessionKey, encryptedData, iv);
		if(userInfo==null){
			result.setMsg("小程序获取用户信息失败");
			result.setCode(1);
			log.info(mapper.toJson(result));
			return this.mapper.toJson(result);
		}
		log.info("获取到的微信用户数据 "+userInfo.toString());
		Member member = memberService.get(userInfo.getOpenId());
		if(member==null){
			result.setMsg("请先调用登录接口");
			result.setCode(1);
			log.info(mapper.toJson(result));
			return this.mapper.toJson(result);
		}
		if(StringUtils.isBlank(member.getNickName())){
			member.setNickName(cn.gzjp.common.utils.StringUtils.filterWxNickName(userInfo.getNickName()));
		}
		member.setAvatarUrl(userInfo.getAvatarUrl());
		memberService.save(member);
		/**
		 * 默认关注官方
		 */
		MemberFollow memberFollow = new MemberFollow();
		memberFollow.setMemberId(userInfo.getOpenId());
		memberFollow.setType("3");
		memberFollow.setFollowId(Global.getOfficialOpenid());
		List<MemberFollow> memberFollows = memberFollowService.findList(memberFollow);
		if(memberFollows==null||memberFollows.size()<=0){
			memberFollowService.save(memberFollow);
			Member official = memberService.get(Global.getOfficialOpenid());
			official.setFansCount(official.getFansCount()+1);
			memberService.save(official);
		}

		result.setData(member);
		log.info(mapper.toJson(result));
		return this.mapper.toJson(result);
	}


	/**
	 * 关注
	 */
	@ApiOperation(value = "关注")
	@RequestMapping(value = "follow", method = RequestMethod.POST)
	@ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "followId", value = "被关住的id", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "type", value = "关注类型(1话题2普通动态3人)", dataType = "String", paramType = "form", required = true)
	})
	@ResponseBody
	public String follow(String openid,String followId,String type) {
		CommonJson json = new CommonJson();
		log.info("关注 openid=" + openid+"|followId="+followId+"|type(1话题2普通动态3人)="+type);
		if(StringUtils.isBlank(openid)){
			CommonJson json1 = new CommonJson();
			json1.setCode(1);
			json1.setMsg("openid不能为空");
			log.info("返回微信小程序" + mapper.toJson(json1));
			return this.mapper.toJson(json1);
		}
		MemberFollow memberFollow = new MemberFollow();
		memberFollow.setMemberId(openid);
		memberFollow.setType(type);
		memberFollow.setFollowId(followId);
		List<MemberFollow> list = memberFollowService.findList(memberFollow);
		if(list!=null&&list.size()>0){
			memberFollow = list.get(0);
			json.setCode(1);
			json.setMsg("您已关注，无需重复关注");
		}
		memberFollowService.save(memberFollow);
		Member member = memberService.get(openid);
		member.setFollowCount(member.getFollowCount()+1);
		memberService.save(member);
		if("3".equals(type)){
			member = memberService.get(followId);
			if(member!=null){
				member.setFansCount(member.getFansCount()+1);
				memberService.save(member);
			}
		}
		json.setData(memberFollow);
		log.info("返回微信小程序" + mapper.toJson(json));
		return this.mapper.toJson(json);
	}

	/**
	 * 取消关注
	 */
	@ApiOperation(value = "取消关注")
	@RequestMapping(value = "unfollow", method = RequestMethod.POST)
	@ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "followId", value = "取消关注的id", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "type", value = "关注类型(1话题2普通动态3人)", dataType = "String", paramType = "form", required = true)
	})
	@ResponseBody
	public String unfollow(String openid,String followId,String type) {
		CommonJson json = new CommonJson();
		log.info("取消关注 openid=" + openid+"|followId="+followId+"|type(1话题2普通动态3人)="+type);
		if(StringUtils.isBlank(openid)){
			CommonJson json1 = new CommonJson();
			json1.setCode(1);
			json1.setMsg("openid不能为空");
			log.info("返回微信小程序" + mapper.toJson(json1));
			return this.mapper.toJson(json1);
		}
		MemberFollow memberFollow = new MemberFollow();
		memberFollow.setMemberId(openid);
		memberFollow.setType(type);
		memberFollow.setFollowId(followId);
		List<MemberFollow> list = memberFollowService.findList(memberFollow);
		if(list!=null&&list.size()>0){
			for(MemberFollow mf:list){
				memberFollowService.delete(mf);
			}
			Member member = memberService.get(openid);
			int followCount = member.getFollowCount()-1;
			if(followCount<0){
				followCount=0;
			}
			member.setFollowCount(followCount);//关注量-1
			memberService.save(member);
			if("3".equals(type)){
				member = memberService.get(followId);
				if(member!=null){
					int fansCount = member.getFansCount()-1;
					if(fansCount<0){
						fansCount=0;
					}
					member.setFansCount(fansCount);
					memberService.save(member);
				}
			}
		}
		log.info("返回微信小程序" + mapper.toJson(json));
		return this.mapper.toJson(json);
	}

	/**
	 * 我的关注
	 */
	@ApiOperation(value = "我的关注")
	@RequestMapping(value = "follow/list", method = RequestMethod.POST)
	@ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int", paramType = "form", required = true),
			@ApiImplicitParam(name = "pageSize", value = "每页显示记录数", dataType = "int", paramType = "form", required = true),
			@ApiImplicitParam(name = "type", value = "关注类型(1话题2普通动态3人)", dataType = "String", paramType = "form", required = true)
	})
	@ResponseBody
	public String followList(String openid,int pageNo,int pageSize,String type) {
		PageJson json = new PageJson();
		log.info("我的关注 openid=" + openid+"|pageNo="+pageNo+"|pageSize="+pageSize+"|type(1话题2普通动态3人)="+type);
		if(StringUtils.isBlank(openid)){
			CommonJson json1 = new CommonJson();
			json1.setCode(1);
			json1.setMsg("openid不能为空");
			log.info("返回微信小程序" + mapper.toJson(json1));
			return this.mapper.toJson(json1);
		}
		MemberFollow search = new MemberFollow();
		search.setMemberId(openid);
		search.setType(type);
		Page<MemberFollow> page = new Page<MemberFollow>(pageNo,pageSize);
		List<MemberFollow> memberFollows  = memberFollowService.findPage(page, search).getList();

		if (page.getLast() >= pageNo) {
			if("3".equals(type)){
				List<Member> data = Lists.newArrayList();
				for(MemberFollow follow:memberFollows){
					Member member = memberService.get(follow.getFollowId());
					data.add(member);
				}
				json.setData(data);
			}else{
				List<Dynamic> data = Lists.newArrayList();
				for(MemberFollow follow:memberFollows){
					Dynamic dynamic = dynamicService.get(follow.getFollowId(),openid);
					data.add(dynamic);
				}
				json.setData(data);
			}
		}

		json.setTotalPage(page.getLast());
		json.setPageSize(pageSize);
		json.setPageNo(pageNo);
		json.setType(type);

		log.info("返回微信小程序" + mapper.toJson(json));
		return this.mapper.toJson(json);
	}

	/**
	 * 我的粉丝
	 */
	@ApiOperation(value = "我的粉丝")
	@RequestMapping(value = "fans/list", method = RequestMethod.POST)
	@ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int", paramType = "form", required = true),
			@ApiImplicitParam(name = "pageSize", value = "每页显示记录数", dataType = "int", paramType = "form", required = true)
	})
	@ResponseBody
	public String fansList(String openid,int pageNo,int pageSize) {
		PageJson json = new PageJson();
		log.info("我的粉丝 openid=" + openid+"|pageNo="+pageNo+"|pageSize="+pageSize);
		if(StringUtils.isBlank(openid)){
			CommonJson json1 = new CommonJson();
			json1.setCode(1);
			json1.setMsg("openid不能为空");
			log.info("返回微信小程序" + mapper.toJson(json1));
			return this.mapper.toJson(json1);
		}
		MemberFollow search = new MemberFollow();
		search.setFollowId(openid);
		Page<MemberFollow> page = new Page<MemberFollow>(pageNo,pageSize);
		List<MemberFollow> memberFollows  = memberFollowService.findPage(page, search).getList();
		List<Member> data = Lists.newArrayList();
		if (page.getLast() >= pageNo) {
			for (MemberFollow follow : memberFollows) {
				Member member = memberService.get(follow.getMemberId());
				data.add(member);
			}
		}
		json.setData(data);
		json.setTotalPage(page.getLast());
		json.setPageSize(pageSize);
		json.setPageNo(pageNo);
		log.info("返回微信小程序" + mapper.toJson(json));
		return this.mapper.toJson(json);
	}

	/**
	 * 完善个人信息
	 */
	@ApiOperation(value = "完善个人信息")
	@RequestMapping(value = "updateInfo", method = RequestMethod.POST)
	@ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "name", value = "姓名", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "summary", value = "个人摘要", dataType = "String", paramType = "form", required = true)
	})
	@ResponseBody
	public String updateInfo(String openid, String name,String summary) {
		CommonJson result = new CommonJson();
		log.info("完善个人信息 openid="+openid+"|name="+name+"|summary="+summary);
		if(StringUtils.isBlank(openid)){
			CommonJson json1 = new CommonJson();
			json1.setCode(1);
			json1.setMsg("openid不能为空");
			log.info("返回微信小程序" + mapper.toJson(json1));
			return this.mapper.toJson(json1);
		}
		Member member = memberService.get(openid);
		if(StringUtils.isNotBlank(name)){
			member.setNickName(name);
		}
		if(StringUtils.isNotBlank(summary)){
			member.setSummary(summary);
		}
		memberService.save(member);
		result.setData(member);
		log.info(mapper.toJson(result));
		return this.mapper.toJson(result);
	}

	/**
	 * 个人详情
	 */
	@ApiOperation(value = "个人详情")
	@RequestMapping(value = "info", method = RequestMethod.POST)
	@ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "当前登录用户的openid", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "memberId", value = "会员id", dataType = "String", paramType = "form", required = true)
	})
	@ResponseBody
	public String info(String openid,String memberId) {
		log.info("个人详情信息 openid="+openid+"|memberId="+memberId);
		CommonJson result = new CommonJson();
		if(StringUtils.isBlank(openid)){
			CommonJson json1 = new CommonJson();
			json1.setCode(1);
			json1.setMsg("openid不能为空");
			log.info("返回微信小程序" + mapper.toJson(json1));
			return this.mapper.toJson(json1);
		}
		if(StringUtils.isBlank(memberId)){
			CommonJson json1 = new CommonJson();
			json1.setCode(1);
			json1.setMsg("memberId不能为空");
			log.info("返回微信小程序" + mapper.toJson(json1));
			return this.mapper.toJson(json1);
		}
		Member member = memberService.get(memberId,openid);
		result.setData(member);
		log.info(mapper.toJson(result));
		return this.mapper.toJson(result);
	}


	/**
	 *  根据昵称获取会员
	 */
	@ApiOperation(value = "根据昵称查询用户")
	@RequestMapping(value = "findByName", method = RequestMethod.POST)
	@ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "form", required = true),
			@ApiImplicitParam(name = "nickName", value = "昵称(为空查所有)", dataType = "String", paramType = "form")
	})
	@ResponseBody
	public String findByName(String openid,String nickName) {
		log.info("根据昵称获取会员 openid="+openid+"|nickName="+nickName);
		CommonJson result = new CommonJson();
		if(StringUtils.isBlank(openid)){
			CommonJson json1 = new CommonJson();
			json1.setCode(1);
			json1.setMsg("openid不能为空");
			log.info("返回微信小程序" + mapper.toJson(json1));
			return this.mapper.toJson(json1);
		}
		Member member = new Member();
		member.setNickName(nickName);
		result.setData(memberService.findList(member));
		log.info(mapper.toJson(result));
		return this.mapper.toJson(result);
	}



}
