package cn.gzjp.modules.sc.api.bean;

import java.io.Serializable;

/**
 * @Author zengcp
 * @Date 2018/3/6
 */
public class UploadVO implements Serializable {

    private String url;
    private String filePath;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
