package cn.gzjp.modules.sc.api.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.*;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.sc.api.bean.CommonJson;
import cn.gzjp.modules.sc.api.bean.PageJson;
import cn.gzjp.modules.sc.entity.*;
import cn.gzjp.modules.sc.service.*;
import com.google.common.collect.Lists;
import com.xiaoleilu.hutool.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 微信小程序内容接口
 */
@Controller("frontDynamic")
@Api(value = "内容接口", description = "内容接口")
@RequestMapping("${frontPath}/dynamic")
public class DynamicController extends BaseController {

    @Autowired
    private DynamicService dynamicService;

    @Autowired
    private DynamicOperateService dynamicOperateService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private MemberFollowService memberFollowService;

    /**
     * 官方动态列表
     */
    @ApiOperation(value = "官方动态列表")
    @RequestMapping(value = "officia/list", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页显示记录数", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "keyword", value = "查询关键字", dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "orderBy", value = "排序(hot/latest)", dataType = "String", paramType = "form")
    })
    @ResponseBody
    public String officia(int pageNo, int pageSize, String keyword, String openid,String orderBy) {
        PageJson json = new PageJson();
        log.info("官方动态列表 pageNo=" + pageNo + "|pageSize=" + pageSize + "|keyword=" + keyword +
                "|openid=" + openid +"|orderBy="+orderBy);
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        Dynamic search = new Dynamic();
        search.setStatus("0");//通过
        search.setMemberId(Global.getOfficialOpenid());
        search.setContent(keyword);
        search.setStatus(DynamicStatus.PASS);
        search.setExcludeTopic(true);
        Page<Dynamic> page = new Page<Dynamic>(pageNo, pageSize);
        page.setOrderBy("a.comment_count desc ");
        if("latest".equals(orderBy)){
            page.setOrderBy("a.create_date desc ");
        }
        Page<Dynamic> pageDate = dynamicService.findPage(page, search);
        List<Dynamic> resultList = Lists.newArrayList();
        if (page.getLast() >= pageNo) {
            for (Dynamic dynamic : pageDate.getList()) {
                resultList.add(dynamicService.get(dynamic.getId(), openid));
            }
        }
        json.setTotalPage(pageDate.getLast());
        json.setPageSize(pageSize);
        json.setPageNo(pageNo);
        json.setData(resultList);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }


    /**
     * 热门动态列表
     */
    @ApiOperation(value = "热门动态列表")
    @RequestMapping(value = "hot/list", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页显示记录数", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "keyword", value = "查询关键字", dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "memberId", value = "会员id", dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "orderBy", value = "排序(hot/latest)", dataType = "String", paramType = "form")
    })
    @ResponseBody
    public String hot(int pageNo, int pageSize, String keyword, String openid, String memberId,String orderBy) {
        PageJson json = new PageJson();
        log.info("热门内容列表 pageNo=" + pageNo + "|pageSize=" + pageSize + "|keyword=" + keyword +
                "|openid=" + openid + "|memberId=" + memberId+"|orderBy="+orderBy);
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        Dynamic search = new Dynamic();
        search.setStatus("0");//通过
        search.setMemberId(memberId);
        search.setContent(keyword);
        search.setStatus(DynamicStatus.PASS);
        search.setExcludeTopic(true);
        Page<Dynamic> page = new Page<Dynamic>(pageNo, pageSize);
        page.setOrderBy("a.comment_count desc ");
        if("latest".equals(orderBy)){
            page.setOrderBy("a.create_date desc ");
        }
        Page<Dynamic> pageDate = dynamicService.findPage(page, search);
        List<Dynamic> resultList = Lists.newArrayList();
        if (page.getLast() >= pageNo) {
            for (Dynamic dynamic : pageDate.getList()) {
                resultList.add(dynamicService.get(dynamic.getId(), openid));
            }
        }
        json.setTotalPage(pageDate.getLast());
        json.setPageSize(pageSize);
        json.setPageNo(pageNo);
        json.setData(resultList);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }


    /**
     * 最新动态列表(只返回自己的和关注过的用户的动态4月18日调整)
     * @param pageNo
     * @param pageSize
     * @param keyword
     * @param openid
     * @param memberId
     * @return
     */
    @ApiOperation(value = "最新动态列表")
    @RequestMapping(value = "latest/list", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页显示记录数", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "keyword", value = "查询关键字", dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "memberId", value = "会员id", dataType = "String", paramType = "form")
    })
    @ResponseBody
    public String latest(int pageNo, int pageSize, String keyword, String openid, String memberId) {
        PageJson json = new PageJson();
        log.info("最新内容列表 pageNo=" + pageNo + "|pageSize=" + pageSize + "|keyword=" + keyword + "|openid=" + openid + "|memberId=" + memberId);
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }



        Dynamic search = new Dynamic();
        search.setStatus("0");//通过
        if(StrUtil.isBlank(memberId)){
            List<String> includeMemberIds = Lists.newArrayList();
            includeMemberIds.add(openid);
            MemberFollow memberFollow = new MemberFollow();
            memberFollow.setMemberId(openid);
            memberFollow.setType("3");
            List<MemberFollow> follows = memberFollowService.findList(memberFollow);
            if(follows!=null&&follows.size()>0){
                for(MemberFollow follow:follows){
                    includeMemberIds.add(follow.getFollowId());
                }
            }
            search.setIncludeMemberIds(includeMemberIds);//memberId 为空时，调整成 返回自己的和关注过的用户的动态
        }else{
            search.setMemberId(memberId);
        }
        search.setStatus(DynamicStatus.PASS);
        search.setContent(keyword);
        search.setExcludeTopic(true);
        Page<Dynamic> page = new Page<Dynamic>(pageNo, pageSize);
        Page<Dynamic> pageDate = dynamicService.findPage(page, search);
        List<Dynamic> resultList = Lists.newArrayList();
        if (page.getLast() >= pageNo) {
            for (Dynamic dynamic : pageDate.getList()) {
                resultList.add(dynamicService.get(dynamic.getId(), openid));
            }
        }
        json.setTotalPage(pageDate.getLast());
        json.setPageSize(pageSize);
        json.setPageNo(pageNo);
        json.setData(resultList);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }

    /**
     * 原创列表
     */
    @ApiOperation(value = "原创动态列表")
    @RequestMapping(value = "original/list", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页显示记录数", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "keyword", value = "查询关键字", dataType = "String", paramType = "form"),
            @ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "memberId", value = "会员id", dataType = "String", paramType = "form")
    })
    @ResponseBody
    public String original(int pageNo, int pageSize, String keyword, String openid, String memberId) {
        PageJson json = new PageJson();
        log.info("原创动态列表 pageNo=" + pageNo + "|pageSize=" + pageSize + "|keyword=" + keyword + "|openid=" + openid + "|memberId=" + memberId);
        Dynamic search = new Dynamic();
        search.setStatus("0");//通过
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        search.setMemberId(memberId);
        search.setContent(keyword);
        search.setOriginal(true);
        search.setExcludeTopic(true);
        search.setStatus(DynamicStatus.PASS);
        Page<Dynamic> page = new Page<Dynamic>(pageNo, pageSize);
        Page<Dynamic> pageDate = dynamicService.findPage(page, search);
        List<Dynamic> resultList = Lists.newArrayList();
        if (page.getLast() >= pageNo) {
            for (Dynamic dynamic : pageDate.getList()) {
                resultList.add(dynamicService.get(dynamic.getId(), openid));
            }
        }
        json.setTotalPage(pageDate.getLast());
        json.setPageSize(pageNo);
        json.setPageNo(pageSize);
        json.setData(resultList);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }


    /**
     * 检索
     */
    @ApiOperation(value = "检索")
    @RequestMapping(value = "search", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "memberId", value = "会员id", dataType = "String", paramType = "form")
    })
    @ResponseBody
    public String search(String keyword, String openid,String memberId) {
        CommonJson json = new CommonJson();
        log.info("检索 keyword=" + keyword + "|openid=" + openid+"|memberId="+memberId);
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        Dynamic search = new Dynamic();
        search.setStatus("0");//通过
        search.setContent(keyword);
        search.setExcludeTopic(true);
        if(StringUtils.isNotBlank(memberId)){
            search.setMemberId(memberId);
        }
        search.setStatus(DynamicStatus.PASS);
        List<Dynamic> dynamicList = dynamicService.findList(search);
        List<Dynamic> resultList = Lists.newArrayList();
        for (Dynamic dynamic : dynamicList) {
            resultList.add(dynamicService.get(dynamic.getId(), openid));
        }
        json.setData(resultList);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }

    /**
     * 详情
     */
    @ApiOperation(value = "详情")
    @RequestMapping(value = "detail", method = RequestMethod.GET)
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "动态内容id", dataType = "String", paramType = "query", required = true),
            @ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "query", required = true)
    })
    @ResponseBody
    public String detail(String id, String openid) {
        CommonJson json = new CommonJson();
        log.info("内容详情 id=" + id + "openid=" + openid);
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        Dynamic dynamic = dynamicService.get(id, openid);
        json.setData(dynamic);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }


    /**
     * 删除
     */
    @ApiOperation(value = "删除")
    @RequestMapping(value = "delete", method = RequestMethod.GET)
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "动态内容id", dataType = "String", paramType = "query", required = true),
            @ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "query", required = true)
    })
    @ResponseBody
    public String delete(String id, String openid) {
        CommonJson json = new CommonJson();
        log.info("内容删除 id=" + id + "openid=" + openid);
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        Dynamic dynamic = dynamicService.get(id);
        if(!openid.equals(dynamic.getMemberId())){
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("无法删除他人动态");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        dynamicService.delete(dynamic);
        Member member = memberService.get(openid);
        if(member.getDynamicCount()>0){
            member.setDynamicCount(member.getDynamicCount()-1);
            memberService.save(member);
        }
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }

    /**
     * 发表动态
     *
     * @param openid
     * @param content
     * @param filePaths
     * @return
     */
    @ApiOperation(value = "发表动态")
    @RequestMapping(value = "publish", method = RequestMethod.POST)
    @ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "content", value = "内容", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "filePaths", value = "上传后的文件路径", dataType = "String[]", paramType = "form", required = true),
            @ApiImplicitParam(name = "atMemberIds", value = "@的用户id(字符数组)", dataType = "String[]", paramType = "form"),
            @ApiImplicitParam(name = "topicNames", value = "话题名称(字符数组)", dataType = "String[]", paramType = "form")

    })
    @ResponseBody
    public String publish(String openid, String content, String[] filePaths,String[] atMemberIds,String[]topicNames) {
        CommonJson result = new CommonJson();
        log.info("内容发布 openid=" + openid + "|content=" + content + "|filePaths=" + filePaths);
        if (StringUtils.isBlank(openid)) {
            result.setMsg("参数openid不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        if (StringUtils.isBlank(content)) {
            result.setMsg("参数content不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        Dynamic dynamic = new Dynamic();
        dynamic.setContent(content);
        dynamic.setMemberId(openid);
        dynamic.setType(DynamicType.CNT);
//        /**
//         * 判断是否为话题
//         */
//        String regex = "#(.*)#";
//        Pattern pattern = Pattern.compile(regex);
//        Matcher matcher = pattern.matcher(content);
//        while (matcher.find()) {
//            String name = matcher.group(1);
//            cnt.setType(DynamicType.TOPIC);
//            cnt.setName(name);
//        }
        dynamic.setStatus(DynamicStatus.PASS);
        dynamic.setAuditWay(AuditWay.AUTO);
        dynamic = dynamicService.createDynamic(dynamic, filePaths, openid,atMemberIds,topicNames);
        dynamic = dynamicService.get(dynamic.getId());
        result.setData(dynamic);
        log.info("返回给小程序数据:" + this.mapper.toJson(result));
        return this.mapper.toJson(result);
    }

    /**
     * 评论动态
     *
     * @param openid
     * @param content
     * @return
     */
    @ApiOperation(value = "评论动态")
    @RequestMapping(value = "comment", method = RequestMethod.POST)
    @ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "content", value = "评论内容", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "dynamicId", value = "评论的动态id", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "atMemberIds", value = "@的用户id(字符数组)", dataType = "String[]", paramType = "form", required = true)
    })
    @ResponseBody
    public String comment(String openid, String content, String dynamicId,String[] atMemberIds) {
        CommonJson result = new CommonJson();
        log.info("内容评论 openid=" + openid + "|content=" + content + "|dynamicId=" + dynamicId);
        if (StringUtils.isBlank(openid)) {
            result.setMsg("参数openid不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        if (StringUtils.isBlank(content)) {
            result.setMsg("参数content不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        if (StringUtils.isBlank(dynamicId)) {
            result.setMsg("参数dynamicId不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        DynamicOperate operate = dynamicOperateService.comment(dynamicId,content,openid,atMemberIds);
        result.setData(operate);
        log.info("返回给小程序数据:" + this.mapper.toJson(result));
        return this.mapper.toJson(result);
    }

    /**
     * 转发动态
     *
     * @param openid
     * @param content
     * @return
     */
    @ApiOperation(value = "转发")
    @RequestMapping(value = "forward", method = RequestMethod.POST)
    @ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "content", value = "内容", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "dynamicId", value = "被转发的动态id", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "atMemberIds", value = "@的用户id(字符数组)", dataType = "String[]", paramType = "form", required = true),
            @ApiImplicitParam(name = "topicNames", value = "话题名称(字符数组)", dataType = "String[]", paramType = "form")
    })
    @ResponseBody
    public String forward(String openid, String content, String dynamicId,String[] atMemberIds,String[]topicNames) {
        CommonJson result = new CommonJson();
        log.info("转发 openid=" + openid + "|content=" + content + "|dynamicId=" + dynamicId);
        if (StringUtils.isBlank(openid)) {
            result.setMsg("参数openid不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }

        if (StringUtils.isBlank(dynamicId)) {
            result.setMsg("参数dynamicId不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        Dynamic dynamic = new Dynamic();
        dynamic.setMemberId(openid);
        dynamic.setOriginalId(dynamicId);
        dynamic.setContent(content);
        dynamic.setType(DynamicType.FORWARD);
        dynamic.setStatus(DynamicStatus.PASS);
        dynamic.setAuditWay(AuditWay.AUTO);
        dynamicService.forwardDynamic(dynamic, openid,atMemberIds,topicNames);
        log.info("返回给小程序数据:" + this.mapper.toJson(result));
        return this.mapper.toJson(result);
    }

    /**
     * 评论并转发
     *
     * @param openid
     * @param content
     * @return
     */
    @ApiOperation(value = "评论并转发")
    @RequestMapping(value = "commentAndForward", method = RequestMethod.POST)
    @ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "content", value = "评论内容", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "dynamicId", value = "评论的动态id", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "atMemberIds", value = "@的用户id(字符数组)", dataType = "String[]", paramType = "form", required = true)
    })
    @ResponseBody
    public String commentAndForward(String openid, String content, String dynamicId,String[] atMemberIds) {
        CommonJson result = new CommonJson();
        log.info("评论并转发 openid=" + openid + "|content=" + content + "|dynamicId=" + dynamicId);
        if (StringUtils.isBlank(openid)) {
            result.setMsg("参数openid不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        if (StringUtils.isBlank(content)) {
            result.setMsg("参数content不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        if (StringUtils.isBlank(dynamicId)) {
            result.setMsg("参数dynamicId不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        DynamicOperate operate = dynamicOperateService.comment(dynamicId,content,openid,atMemberIds);//评论
        Dynamic dynamic = new Dynamic();
        dynamic.setMemberId(openid);
        dynamic.setOriginalId(dynamicId);
        dynamic.setContent(content);
        dynamic.setType(DynamicType.FORWARD);
        dynamic.setStatus(DynamicStatus.PASS);
        dynamic.setAuditWay(AuditWay.AUTO);
        dynamicService.forwardDynamic(dynamic, openid,atMemberIds,null);//转发

        result.setData(operate);
        log.info("返回给小程序数据:" + this.mapper.toJson(result));
        return this.mapper.toJson(result);
    }

    /**
     *  根据名称查询话题
     */
    @ApiOperation(value = "根据名称查询话题")
    @RequestMapping(value = "findTopicByName", method = RequestMethod.POST)
    @ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "topicName", value = "话题", dataType = "String", paramType = "form")
    })
    @ResponseBody
    public String findTopicByName(String openid,String topicName) {
        log.info("根据名称查询话题 openid="+openid+"|topicName="+topicName);
        CommonJson result = new CommonJson();
        if(StringUtils.isBlank(openid)){
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        Dynamic search = new Dynamic();
        search.setType(DynamicType.TOPIC);
        search.setTopicNames(topicName);
        result.setData(dynamicService.findList(search));
        log.info(mapper.toJson(result));
        return this.mapper.toJson(result);
    }

    /**
     * 删除评论
     */
    @ApiOperation(value = "删除评论")
    @RequestMapping(value = "deleteComment", method = RequestMethod.GET)
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "评论id", dataType = "String", paramType = "query", required = true),
            @ApiImplicitParam(name = "openid", value = "当前登录用户openid", dataType = "String", paramType = "query", required = true)
    })
    @ResponseBody
    public String deleteComment(String id, String openid) {
        CommonJson json = new CommonJson();
        log.info("删除评论 id=" + id + "openid=" + openid);
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        DynamicOperate comment = dynamicOperateService.get(id);
        if(!openid.equals(comment.getMemberId())){
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("无法删除他人评论");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        dynamicOperateService.delete(comment);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }


}
