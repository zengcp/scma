package cn.gzjp.modules.sc.api.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 分页列表
 * @author jp_zh
 *
 */
public class PageJson implements Serializable {
	
	private static final long serialVersionUID = -3758568680385471658L;
	
	private String type;
	private int pageNo;
	private int pageSize;
	private int totalPage;//总页数
	private List data;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List getData() {
		return data;
	}

	public void setData(List data) {
		this.data = data;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
}
