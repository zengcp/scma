package cn.gzjp.modules.sc.api.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.DynamicStatus;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.sc.api.bean.CommonJson;
import cn.gzjp.modules.sc.api.bean.PageJson;
import cn.gzjp.modules.sc.api.bean.UploadVO;
import cn.gzjp.modules.sc.entity.Dynamic;
import cn.gzjp.modules.sc.entity.DynamicOperate;
import cn.gzjp.modules.sc.entity.Message;
import cn.gzjp.modules.sc.service.DynamicOperateService;
import cn.gzjp.modules.sc.service.DynamicService;
import cn.gzjp.modules.sc.service.MessageService;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 消息接口
 * @author zengcp
 */
@Controller("frontMessage")
@Api(value = "消息接口", description = "消息接口")
@RequestMapping("${frontPath}/message")
public class MessageController extends BaseController {


    @Autowired
    private MessageService messageService;
    @Autowired
    private DynamicService dynamicService;
    @Autowired
    private DynamicOperateService dynamicOperateService;


    /**
     *
     */
    /**
     * 发送消息
     */
    @ApiOperation(value = "@消息发送")
    @RequestMapping(value = "send", method = RequestMethod.POST)
    @ApiImplicitParams({@ApiImplicitParam(name = "openid", value = "openid", dataType = "form", paramType = "form", required = true),
            @ApiImplicitParam(name = "receiverId", value = "消息接收者id(@用户)", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "content", value = "消息内容", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "dynamicId", value = "动态id", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "type", value = "类型(1 动态 2原创 3评论)", dataType = "String", paramType = "form", required = true)
    })
    @ResponseBody
    public String send(String openid,String receiverId, String content,String dynamicId,String type) {
        log.info("消息发送 openid="+openid+"|receiverId="+receiverId+"|content="+content+"|dynamicId="+dynamicId+"|type="+type);
        CommonJson result = new CommonJson();
        if (StringUtils.isBlank(openid)) {
            result.setMsg("参数openid不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        if (StringUtils.isBlank(receiverId)) {
            result.setMsg("参数receiverId不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        if (StringUtils.isBlank(dynamicId)) {
            result.setMsg("参数dynamicId不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        if (StringUtils.isBlank(type)) {
            result.setMsg("参数type不能为空");
            result.setCode(1);
            log.info("返回给小程序数据:" + this.mapper.toJson(result));
            return this.mapper.toJson(result);
        }
        Message message = new Message();
        message.setSenderId(openid);
        message.setReceiverId(receiverId);
        message.setContent(content);
        message.setStatus("0");
        message.setDynamicId(dynamicId);
        message.setType(type);
        messageService.save(message);
        result.setData(message);
        log.info("消息发送返回:" + this.mapper.toJson(result));
        return this.mapper.toJson(result);
    }



    /**
     * @我列表
     */
    @ApiOperation(value = "@我列表")
    @RequestMapping(value = "list", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页显示记录数", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "openid", value = "我的openid", dataType = "String", paramType = "form", required = true),
            @ApiImplicitParam(name = "type", value = "类型(1 动态 2原创 3评论)", dataType = "String", paramType = "form", required = true)
    })
    @ResponseBody
    public String list(int pageNo, int pageSize,  String openid,String type) {
        PageJson json = new PageJson();
        log.info("@我列表 pageNo=" + pageNo + "|pageSize=" + pageSize + " |openid="+openid+"|type="+type );
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        Message search = new Message();
        search.setReceiverId(openid);
        search.setType(type);
        Page<Message> page = new Page<Message>(pageNo, pageSize);
        Page<Message> pageDate = messageService.findPage(page, search);
       // List<Message> resultList = Lists.newArrayList();
       if(!"3".equals(type)){
           List<Dynamic> resultList = Lists.newArrayList();
           if (page.getLast() >= pageNo) {
               for (Message message : pageDate.getList()) {
                   resultList.add(dynamicService.get(message.getDynamicId(),openid));
               }
           }
           json.setData(resultList);
       }else{
           List<DynamicOperate> resultList = Lists.newArrayList();
           if (page.getLast() >= pageNo) {
               for (Message message : pageDate.getList()) {
                   if(StringUtils.isNotBlank(message.getOperateId())){
                       resultList.add(dynamicOperateService.get(message.getOperateId()));
                   }
               }
           }
           json.setData(resultList);
       }
        json.setTotalPage(pageDate.getLast());
        json.setPageSize(pageSize);
        json.setPageNo(pageNo);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }

    /**
     * 我发的评论
     */
    @ApiOperation(value = "我发的评论")
    @RequestMapping(value = "mySends", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页显示记录数", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "openid", value = "我的openid", dataType = "String", paramType = "form", required = true),
    })
    @ResponseBody
    public String mySends(int pageNo, int pageSize,  String openid) {
        PageJson json = new PageJson();
        log.info("我发的评论 pageNo=" + pageNo + "|pageSize=" + pageSize + " |openid="+openid);
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }
        DynamicOperate search = new DynamicOperate();
        search.setStatus("0");
        search.setMemberId(openid);
        Page<DynamicOperate> page = new Page<DynamicOperate>(pageNo, pageSize);
        Page<DynamicOperate> pageDate = dynamicOperateService.findPage(page, search);
        List<DynamicOperate> resultList = Lists.newArrayList();
        if (page.getLast() >= pageNo) {
            for (DynamicOperate operate : pageDate.getList()) {
                resultList.add(dynamicOperateService.get(operate.getId()));
            }
        }
        json.setTotalPage(pageDate.getLast());
        json.setPageSize(pageSize);
        json.setPageNo(pageNo);
        json.setData(resultList);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }

    /**
     * 我收到的评论
     */
    @ApiOperation(value = "我收到的评论")
    @RequestMapping(value = "myReceives", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页显示记录数", dataType = "int", paramType = "form", required = true),
            @ApiImplicitParam(name = "openid", value = "我的openid", dataType = "String", paramType = "form", required = true),
    })
    @ResponseBody
    public String myReceives(int pageNo, int pageSize,  String openid) {
        PageJson json = new PageJson();
        log.info("我收到的评论 pageNo=" + pageNo + "|pageSize=" + pageSize + " |openid="+openid);
        if (StringUtils.isBlank(openid)) {
            CommonJson json1 = new CommonJson();
            json1.setCode(1);
            json1.setMsg("openid不能为空");
            log.info("返回微信小程序" + mapper.toJson(json1));
            return this.mapper.toJson(json1);
        }

        DynamicOperate search = new DynamicOperate();
        search.setStatus("0");
        search.setReceiverId(openid);
        Page<DynamicOperate> page = new Page<DynamicOperate>(pageNo, pageSize);
        Page<DynamicOperate> pageDate = dynamicOperateService.findPage(page, search);
        List<DynamicOperate> resultList = Lists.newArrayList();
        if (page.getLast() >= pageNo) {
            for (DynamicOperate operate : pageDate.getList()) {
                resultList.add(dynamicOperateService.get(operate.getId()));
            }
        }
        json.setTotalPage(pageDate.getLast());
        json.setPageSize(pageSize);
        json.setPageNo(pageNo);
        json.setData(resultList);
        log.info("返回微信小程序" + mapper.toJson(json));
        return this.mapper.toJson(json);
    }

}