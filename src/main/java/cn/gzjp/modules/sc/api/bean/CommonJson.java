package cn.gzjp.modules.sc.api.bean;


import java.io.Serializable;


/**
 * 通用成功返回json
 * @author zengcp
 *
 */
public class CommonJson implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4881094516971405192L;
	private int code;
	private String msg;
	public Object data;
	
	public CommonJson(){
		this.setCode(0);
		this.setMsg("操作成功");
	}

	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}


	public Object getData() {
		return data;
	}


	public void setData(Object data) {
		this.data = data;
	}

}
