package cn.gzjp.modules.sc.api.web;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import cn.gzjp.common.config.Global;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.sc.api.bean.CommonJson;
import cn.gzjp.modules.sc.api.bean.PageJson;
import cn.gzjp.modules.sc.entity.Background;
import cn.gzjp.modules.sc.entity.Dynamic;
import cn.gzjp.modules.sc.entity.Member;
import cn.gzjp.modules.sc.entity.MemberFollow;
import cn.gzjp.modules.sc.service.*;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.exception.WxErrorException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * 微信小程序用户接口
 *
 */
@Controller("backgroundFrontController")
@Api(value = "背景接口", description = "背景接口")
@RequestMapping("${frontPath}/background")
public class BackgroundController extends BaseController {

    WxMaService wxService = WxService.getWxMaService();
    @Autowired
    private MemberService memberService;
	@Autowired
    private MemberFollowService memberFollowService;
	@Autowired
	private BackgroundService backgroundService;
	@Autowired
	private DynamicService dynamicService;
	@Autowired
	private MessageService messageService;
    



	/**
	 * 背景信息
	 */
	@ApiOperation(value = "小程序背景信息")
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public String background() {
		CommonJson result = new CommonJson();
		log.info("背景信息获取");
		Background background = backgroundService.get("0");
		result.setData(background);
		log.info(mapper.toJson(result));
		return this.mapper.toJson(result);
	}


}
