package cn.gzjp.modules.sc.api.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.sc.api.bean.CommonJson;
import cn.gzjp.modules.sc.api.bean.UploadVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 文件上传管理
 * @author zengcp
 */
@Controller("frontUpload")
@Api(value = "图片或视频上传", description = "图片或视频上传")
@RequestMapping("${frontPath}/upload")
public class UploadController extends BaseController {


    /**
     * 图片或视频上传
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "图片或视频上传")
    @RequestMapping(value = "file", method = RequestMethod.POST)
    @ResponseBody
    public String file(MultipartFile file) throws Exception {
        CommonJson result = new CommonJson();
        // 获取文件需要上传到的路径
        String path = Global.getFileUploadPath() + "/userfiles/media/";
        String fileName = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        String destPath = path + fileName;

        log.info("图片或视频上传到=" + path);
        if (file == null) {
            result.setCode(1);
            result.setMsg("file参数不能为空");
            return mapper.toJson(result);
        }

        if (!file.isEmpty()) {
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
            File outPutFile = new File(destPath + "." + extension);
            FileUtils.writeByteArrayToFile(outPutFile, file.getBytes());
            String filePath = "/userfiles/media/" +fileName + "." + extension;
            String url = Global.getConfig("photoUrl") +filePath;
            UploadVO vo = new UploadVO();
            vo.setFilePath(filePath);
            vo.setUrl(url);
            result.setData(vo);
        }
        log.info("图片或视频上传返回:" + this.mapper.toJson(result));
        return this.mapper.toJson(result);
    }


}