package cn.gzjp.modules.sc.entity;

import cn.gzjp.common.config.ConfigUtils;
import cn.gzjp.common.config.Global;
import cn.gzjp.common.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 图片视频Entity
 * @author zengcp
 * @version 2018-03-06
 */
public class DynamicMedia extends DataEntity<DynamicMedia> {
	
	private static final long serialVersionUID = 1L;
	private String dynamicId;		// 内容id
	private String path;		// 图片或视频存放路径

	private String mediaUrl;    //访问路径

	private String conclusion;//百度接口审核结论 不合规/合规
	private String msg;//提示信息

	private String uncensor;//未审核


	private int sort;
	
	public DynamicMedia() {
		super();
	}

	public DynamicMedia(String id){
		super(id);
	}

	public String getDynamicId() {
		return dynamicId;
	}

	public void setDynamicId(String dynamicId) {
		this.dynamicId = dynamicId;
	}

	@JsonIgnore
	@Length(min=0, max=256, message="存放路径长度必须介于 0 和 256 之间")
	@ExcelField(title="存放路径", align=2, sort=2)
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getMediaUrl() {
		if (StringUtils.isNotBlank(path)) {
			if(path.startsWith("http")){
				path = path.replaceAll("scma/scma","scma");
				return path;
			}else{
				String photoUrl = ConfigUtils.getProperty("photoUrl");
				path =  photoUrl + path;
				path = path.replaceAll("scma/scma","scma");
				return path;
			}
		}
		return "";
	}

	public void setMediaUrl(String mediaUrl) {
		this.mediaUrl = mediaUrl;
	}

	public String getConclusion() {
		return conclusion;
	}

	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@JsonIgnore
	public String getUncensor() {
		return uncensor;
	}
	@JsonIgnore
	public String getMediaFilePath(){
		if((StringUtils.isNotBlank(this.getPath()))&&this.getPath().startsWith("/userfiles")){
			return Global.getFileUploadPath()+this.getPath();
		}
		return this.getPath();
	}

	public void setUncensor(String uncensor) {
		this.uncensor = uncensor;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}
}