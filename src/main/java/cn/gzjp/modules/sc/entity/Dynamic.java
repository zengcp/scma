package cn.gzjp.modules.sc.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

import java.util.List;

/**
 * 动态管理Entity
 * @author zengcp
 * @version 2018-03-09
 */
public class Dynamic extends DataEntity<Dynamic> {
	
	private static final long serialVersionUID = 1L;
	private String topicNames;		// 话题名称
	private String topicIds;//话题id
	private String content;		// 内容
	private String memberId;		// 会员id
	private String memberName;      //会员名称
	private String memberHeadImg;   //会员头像
	private int commentCount;		// 评论量
	private int forwardCount;		// 转发量
	private int collectionCount;		// 收藏量
	private String type;		// 1话题2动态3转发
	private String originalId;		// 原创id
	private Dynamic originalDynamic;//原创动态;
	private String status;		// 状态 0通过 1不通过2待审核
	private String auditWay;		// 1自动2人工

	private String atMeberIds;//@id 多个|分割
	private String atMemberNames; //@了的名字  多个|分割

	private List<DynamicMedia> mediaList;

	private String pics;		// 图片

	private List<DynamicOperate> commentList;//评论列表
	private List<Dynamic> forwardList;//转发列表

	private boolean isOriginal;//原创动态

	private boolean isCollect;//是否收藏


	private boolean excludeTopic;//排除话题

	private List<String> includeMemberIds;//查询指定会员的

	public Dynamic() {
		super();
	}

	public Dynamic(String id){
		super(id);
	}


	public String getTopicNames() {
		return topicNames;
	}

	public void setTopicNames(String topicNames) {
		this.topicNames = topicNames;
	}

	public String getTopicIds() {
		return topicIds;
	}

	public void setTopicIds(String topicIds) {
		this.topicIds = topicIds;
	}

	@ExcelField(title="内容", align=2, sort=2)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Length(min=0, max=32, message="会员id长度必须介于 0 和 32 之间")
	@ExcelField(title="会员id", align=2, sort=3)
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	@Length(min=0, max=11, message="评论量长度必须介于 0 和 11 之间")
	@ExcelField(title="评论量", align=2, sort=4)
	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	
	@Length(min=0, max=11, message="转发量长度必须介于 0 和 11 之间")
	@ExcelField(title="转发量", align=2, sort=5)
	public int getForwardCount() {
		return forwardCount;
	}

	public void setForwardCount(int forwardCount) {
		this.forwardCount = forwardCount;
	}
	
	@Length(min=0, max=11, message="收藏量长度必须介于 0 和 11 之间")
	@ExcelField(title="收藏量", align=2, sort=6)
	public int getCollectionCount() {
		return collectionCount;
	}

	public void setCollectionCount(int collectionCount) {
		this.collectionCount = collectionCount;
	}
	@Length(min=0, max=1, message="1话题2动态3转发长度必须介于 0 和 1 之间")
	@ExcelField(title="1话题2动态3转发", dictType="content_type", align=2, sort=7)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	@JsonIgnore
	@Length(min=0, max=32, message="原创id长度必须介于 0 和 32 之间")
	@ExcelField(title="原创id", align=2, sort=8)
	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}

	@JsonIgnore
	@Length(min=0, max=1, message="状态 0通过 1不通过2待审核长度必须介于 0 和 1 之间")
	@ExcelField(title="状态 0通过 1不通过2待审核", dictType="content_status", align=2, sort=14)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonIgnore
	@Length(min=0, max=1, message="1自动2人工长度必须介于 0 和 1 之间")
	@ExcelField(title="1自动2人工", dictType="audit_way", align=2, sort=15)
	public String getAuditWay() {
		return auditWay;
	}

	public void setAuditWay(String auditWay) {
		this.auditWay = auditWay;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberHeadImg() {
		return memberHeadImg;
	}

	public void setMemberHeadImg(String memberHeadImg) {
		this.memberHeadImg = memberHeadImg;
	}

	public List<DynamicMedia> getMediaList() {
		return mediaList;
	}

	public void setMediaList(List<DynamicMedia> mediaList) {
		this.mediaList = mediaList;
	}

	public List<DynamicOperate> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<DynamicOperate> commentList) {
		this.commentList = commentList;
	}

	public Dynamic getOriginalDynamic() {
		return originalDynamic;
	}

	public void setOriginalDynamic(Dynamic originalDynamic) {
		this.originalDynamic = originalDynamic;
	}

	public boolean isOriginal() {
		return isOriginal;
	}

	public void setOriginal(boolean original) {
		isOriginal = original;
	}

	public boolean isCollect() {
		return isCollect;
	}

	public void setCollect(boolean collect) {
		isCollect = collect;
	}

	public List<Dynamic> getForwardList() {
		return forwardList;
	}

	public void setForwardList(List<Dynamic> forwardList) {
		this.forwardList = forwardList;
	}

	public String getAtMeberIds() {
		return atMeberIds;
	}

	public void setAtMeberIds(String atMeberIds) {
		this.atMeberIds = atMeberIds;
	}

	public String getAtMemberNames() {
		return atMemberNames;
	}

	public void setAtMemberNames(String atMemberNames) {
		this.atMemberNames = atMemberNames;
	}

	public boolean isExcludeTopic() {
		return excludeTopic;
	}

	public void setExcludeTopic(boolean excludeTopic) {
		this.excludeTopic = excludeTopic;
	}

	public List<String> getIncludeMemberIds() {
		return includeMemberIds;
	}

	public void setIncludeMemberIds(List<String> includeMemberIds) {
		this.includeMemberIds = includeMemberIds;
	}

	@JsonIgnore
	public String getPics() {
		return pics;
	}

	public void setPics(String pics) {
		this.pics = pics;
	}
}