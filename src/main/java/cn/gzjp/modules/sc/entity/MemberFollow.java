package cn.gzjp.modules.sc.entity;

import org.hibernate.validator.constraints.Length;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 会员关注Entity
 * @author zengcp
 * @version 2018-03-07
 */
public class MemberFollow extends DataEntity<MemberFollow> {
	
	private static final long serialVersionUID = 1L;
	private String memberId;		// 会员id
	private String type;		// 1话题2内容3人
	private String followId;		// 被关注的id
	private String content;		// 内容
	
	public MemberFollow() {
		super();
	}

	public MemberFollow(String id){
		super(id);
	}

	@Length(min=0, max=32, message="会员id长度必须介于 0 和 32 之间")
	@ExcelField(title="会员id", align=2, sort=1)
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	@Length(min=0, max=1, message="1话题2内容3人长度必须介于 0 和 1 之间")
	@ExcelField(title="1话题2内容3人", align=2, sort=2)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Length(min=0, max=32, message="关注的id长度必须介于 0 和 32 之间")
	@ExcelField(title="关注的id", align=2, sort=3)
	public String getFollowId() {
		return followId;
	}

	public void setFollowId(String followId) {
		this.followId = followId;
	}
	
	@ExcelField(title="内容", align=2, sort=4)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}