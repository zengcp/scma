package cn.gzjp.modules.sc.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 内容分类Entity
 * @author zengcp
 * @version 2018-03-04
 */
public class Category extends DataEntity<Category> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 名称
	private Integer no;		// 序号
	private String status;		// 状态 0通过 1不通过
	
	public Category() {
		super();
	}

	public Category(String id){
		super(id);
	}

	@Length(min=0, max=128, message="名称长度必须介于 0 和 128 之间")
	@ExcelField(title="名称", align=2, sort=1)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@ExcelField(title="序号", align=2, sort=2)
	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	@JsonIgnore
	@Length(min=0, max=1, message="状态 0通过 1不通过长度必须介于 0 和 1 之间")
	@ExcelField(title="状态 0通过 1不通过", align=2, sort=9)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}