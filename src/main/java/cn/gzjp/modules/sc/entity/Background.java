package cn.gzjp.modules.sc.entity;

import cn.gzjp.common.config.ConfigUtils;
import cn.gzjp.common.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 背景管理Entity
 * @author zengcp
 * @version 2018-03-04
 */
public class Background extends DataEntity<Background> {
	
	private static final long serialVersionUID = 1L;
	private String color;		// 背景颜色
	private String pic;		// pic

	public Background() {
		super();
	}

	public Background(String id){
		super(id);
	}

	@Length(min=0, max=32, message="背景颜色长度必须介于 0 和 32 之间")
	@ExcelField(title="背景颜色", align=2, sort=1)
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@JsonIgnore
	@Length(min=0, max=256, message="pic长度必须介于 0 和 256 之间")
	@ExcelField(title="pic", align=2, sort=2)
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getPicUrl(){
		String pic = this.getPic();
		if(StringUtils.isNotBlank(pic)){
			String keyPre ="/scma";
			if(pic.startsWith(keyPre)){
				pic = pic.replace("/scma","");
			}
			String photoUrl = ConfigUtils.getProperty("photoUrl");
			return photoUrl + pic;
		}
		return "";
	}
	

	
}