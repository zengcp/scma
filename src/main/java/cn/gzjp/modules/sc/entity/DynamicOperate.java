package cn.gzjp.modules.sc.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 评论@回复评论Entity
 * @author zengcp
 * @version 2018-03-11
 */
public class DynamicOperate extends DataEntity<DynamicOperate> {
	
	private static final long serialVersionUID = 1L;
	private String memberId;		// 评论者id
	private String memberName;      //会员名称
	private String memberHeadImg;   //会员头像
	private String dynamicId;		// 动态id
	private String dynamicContent; //动态内容
	private String type;		// 1评论  2回复评论
	private String content;		// 评论内容
	private String status;		// 状态 0通过 1不通过2待审核
	private String replyContent;//回复评论内容

	private String atMeberIds;
	private String atMemberNames;

	private Dynamic dynamic;//评论的动态

	private String receiverId;//收评论者,动态的作者
	
	public DynamicOperate() {
		super();
	}

	public DynamicOperate(String id){
		super(id);
	}

	@Length(min=0, max=32, message="会员id长度必须介于 0 和 32 之间")
	@ExcelField(title="会员id", align=2, sort=1)
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	@Length(min=0, max=32, message="动态id长度必须介于 0 和 32 之间")
	@ExcelField(title="动态id", align=2, sort=2)
	public String getDynamicId() {
		return dynamicId;
	}

	public void setDynamicId(String dynamicId) {
		this.dynamicId = dynamicId;
	}
	
	@Length(min=0, max=1, message="1评论  2回复评论长度必须介于 0 和 1 之间")
	@ExcelField(title="1评论  2回复评论", align=2, sort=3)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="内容", align=2, sort=4)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Length(min=0, max=1, message="状态 0通过 1不通过2待审核长度必须介于 0 和 1 之间")
	@ExcelField(title="状态 0通过 1不通过2待审核", dictType="content_status", align=2, sort=11)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberHeadImg() {
		return memberHeadImg;
	}

	public void setMemberHeadImg(String memberHeadImg) {
		this.memberHeadImg = memberHeadImg;
	}
	@JsonIgnore
	public String getDynamicContent() {
		return dynamicContent;
	}

	public void setDynamicContent(String dynamicContent) {
		this.dynamicContent = dynamicContent;
	}
	@JsonIgnore
	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public String getAtMeberIds() {
		return atMeberIds;
	}

	public void setAtMeberIds(String atMeberIds) {
		this.atMeberIds = atMeberIds;
	}

	public String getAtMemberNames() {
		return atMemberNames;
	}

	public void setAtMemberNames(String atMemberNames) {
		this.atMemberNames = atMemberNames;
	}

	public Dynamic getDynamic() {
		return dynamic;
	}

	public void setDynamic(Dynamic dynamic) {
		this.dynamic = dynamic;
	}

	public String getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
}