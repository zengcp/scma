package cn.gzjp.modules.sc.entity;

import org.hibernate.validator.constraints.Length;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 消息Entity 用于 @发消息
 * @author zengcp
 * @version 2018-03-11
 */
public class Message extends DataEntity<Message> {
	
	private static final long serialVersionUID = 1L;
	private String content;		// 内容
	private String senderId;		// 消息发送者
	private String senderName;      //发送者名称
	private String senderHeadImg;   //发送者头像
	private String receiverId;		// 消息接受者id
	private String receiverName;      //接受者名称
	private String receiverHeadImg;   //接受者头像
	private String status;		// 状态 0未读 1已读
	private String replyContent;		// 回复内容

	private String dynamicId;//动态id

	private String operateId;//评论id

	private String type;//1 动态 2原创 3评论

	private Dynamic dynamic;//动态内容
	
	public Message() {
		super();
	}

	public Message(String id){
		super(id);
	}

	@ExcelField(title="内容", align=2, sort=1)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Length(min=0, max=32, message="消息发送者长度必须介于 0 和 32 之间")
	@ExcelField(title="消息发送者", align=2, sort=2)
	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	
	@Length(min=0, max=32, message="消息接受者id长度必须介于 0 和 32 之间")
	@ExcelField(title="消息接受者id", align=2, sort=3)
	public String getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	
	@Length(min=0, max=1, message="状态 0未读 1已读长度必须介于 0 和 1 之间")
	@ExcelField(title="状态 0未读 1已读", align=2, sort=6)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@ExcelField(title="回复内容", align=2, sort=7)
	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderHeadImg() {
		return senderHeadImg;
	}

	public void setSenderHeadImg(String senderHeadImg) {
		this.senderHeadImg = senderHeadImg;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverHeadImg() {
		return receiverHeadImg;
	}

	public void setReceiverHeadImg(String receiverHeadImg) {
		this.receiverHeadImg = receiverHeadImg;
	}

	public String getDynamicId() {
		return dynamicId;
	}

	public void setDynamicId(String dynamicId) {
		this.dynamicId = dynamicId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Dynamic getDynamic() {
		return dynamic;
	}

	public void setDynamic(Dynamic dynamic) {
		this.dynamic = dynamic;
	}

	public String getOperateId() {
		return operateId;
	}

	public void setOperateId(String operateId) {
		this.operateId = operateId;
	}
}