package cn.gzjp.modules.sc.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 会员管理Entity
 * @author zengcp
 * @version 2018-03-04
 */
public class Member extends DataEntity<Member> {
	
	private static final long serialVersionUID = 1L;
	private String nickName;		// 昵称
	private String avatarUrl;		// 头像
	private String gender;		// 性别
	private String mobile;		// 手机号
	private int dynamicCount;		// 发表动态数量
	private int followCount;		//关注别人数量
	private int fansCount;//粉丝被别人关注数量
	private String summary; //摘要

	private boolean isFollow=false;//是否关注

	private long messageCount;//消息数量;
	
	public Member() {
		super();
	}

	public Member(String id){
		super(id);
	}

	@Length(min=0, max=128, message="昵称长度必须介于 0 和 128 之间")
	@ExcelField(title="昵称", align=2, sort=1)
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	@Length(min=0, max=256, message="头像长度必须介于 0 和 256 之间")
	@ExcelField(title="头像", align=2, sort=2)
	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	
	@Length(min=0, max=32, message="性别长度必须介于 0 和 32 之间")
	@ExcelField(title="性别", align=2, sort=3)
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Length(min=0, max=11, message="手机号长度必须介于 0 和 11 之间")
	@ExcelField(title="手机号", align=2, sort=4)
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public int getDynamicCount() {
		return dynamicCount;
	}

	public void setDynamicCount(int dynamicCount) {
		this.dynamicCount = dynamicCount;
	}

	public int getFollowCount() {
		return followCount;
	}

	public void setFollowCount(int followCount) {
		this.followCount = followCount;
	}

	public int getFansCount() {
		return fansCount;
	}

	public void setFansCount(int fansCount) {
		this.fansCount = fansCount;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public boolean isFollow() {
		return isFollow;
	}

	public void setFollow(boolean follow) {
		isFollow = follow;
	}

	public long getMessageCount() {
		return messageCount;
	}

	public void setMessageCount(long messageCount) {
		this.messageCount = messageCount;
	}
}