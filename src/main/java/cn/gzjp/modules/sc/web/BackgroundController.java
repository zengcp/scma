package cn.gzjp.modules.sc.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.util.List;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import cn.gzjp.common.utils.DateUtils;
import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.utils.excel.ExportExcel;
import cn.gzjp.common.utils.excel.ImportExcel;
import cn.gzjp.modules.sc.entity.Background;
import cn.gzjp.modules.sc.service.BackgroundService;

/**
 * 背景管理Controller
 * @author zengcp
 * @version 2018-03-04
 */
@Controller
@RequestMapping(value = "${adminPath}/sc/background")
public class BackgroundController extends BaseController {

	@Autowired
	private BackgroundService backgroundService;
	
	
	/**
	 * 背景信息列表页面
	 */
	@RequiresPermissions("sc:background:list")
	@RequestMapping(value = {"list", ""})
	public String list(Background background, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Background> page = backgroundService.findPage(new Page<Background>(request, response), background); 
		model.addAttribute("page", page);
		return "modules/sc/backgroundList";
	}

	/**
	 * 查看，增加，编辑背景信息表单页面
	 */
	@RequiresPermissions(value={"sc:background:view","sc:background:add","sc:background:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Background background, Model model) {
		model.addAttribute("background", background);
		return "modules/sc/backgroundForm";
	}

	/**
	 * 保存背景信息
	 */
	@RequiresPermissions(value={"sc:background:add","sc:background:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(Background background, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, background)){
			return form(background, model);
		}
		if(!background.getIsNewRecord()){//编辑表单保存
			Background t = backgroundService.get(background.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(background, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			backgroundService.save(t);//保存
		}else{//新增表单保存
			backgroundService.save(background);//保存
		}
		addMessage(redirectAttributes, "保存背景信息成功");
		return redirect("/sc/background/?repage");
	}
	
	/**
	 * 删除背景信息
	 */
	@RequiresPermissions("sc:background:del")
	@RequestMapping(value = "delete")
	public String delete(Background background, RedirectAttributes redirectAttributes) {
		backgroundService.delete(background);
		addMessage(redirectAttributes, "删除背景信息成功");
		return redirect("/sc/background/?repage");
	}
	
	/**
	 * 批量删除背景信息
	 */
	@RequiresPermissions("sc:background:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			backgroundService.delete(backgroundService.get(id));
		}
		addMessage(redirectAttributes, "删除背景信息成功");
		return redirect("/sc/background/?repage");
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("sc:background:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(Background background, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "背景信息"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Background> page = backgroundService.findPage(new Page<Background>(request, response, -1), background);
    		new ExportExcel("背景信息", Background.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出背景信息记录失败！失败信息："+e.getMessage());
		}
		return redirect("/sc/background/?repage");
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("sc:background:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Background> list = ei.getDataList(Background.class);
			for (Background background : list){
				try{
					backgroundService.save(background);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条背景信息记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条背景信息记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入背景信息失败！失败信息："+e.getMessage());
		}
		return redirect("/sc/background/?repage");
    }
	
	/**
	 * 下载导入背景信息数据模板
	 */
	@RequiresPermissions("sc:background:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "背景信息数据导入模板.xlsx";
    		List<Background> list = Lists.newArrayList(); 
    		new ExportExcel("背景信息数据", Background.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return redirect("/sc/background/?repage");
    }
	@ModelAttribute
	public Background get(@RequestParam(required=false) String id) {
		Background entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = backgroundService.get(id);
		}
		if (entity == null){
			entity = new Background();
		}
		return entity;
	}
	

}