package cn.gzjp.modules.sc.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.AuditWay;
import cn.gzjp.common.constant.DynamicStatus;
import cn.gzjp.common.constant.DynamicType;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.sc.entity.Dynamic;
import cn.gzjp.modules.sc.entity.DynamicOperate;
import cn.gzjp.modules.sc.entity.Member;
import cn.gzjp.modules.sc.entity.Message;
import cn.gzjp.modules.sc.service.DynamicOperateService;
import cn.gzjp.modules.sc.service.DynamicService;
import cn.gzjp.modules.sc.service.MemberService;
import cn.gzjp.modules.sc.service.MessageService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 内容管理Controller
 * @author zengcp
 * @version 2018-03-04
 */
@Controller
@RequestMapping(value = "${adminPath}/sc/official")
public class OfficialController extends BaseController {

	@Autowired
	private DynamicService dynamicService;
	@Autowired
	private DynamicOperateService dynamicOperateService;
	@Autowired
	private MessageService messageService;

	private final static String officialOpenid = Global.getOfficialOpenid();
	
	
	/**
	 * 内容列表页面
	 */
	@RequiresPermissions("sc:dynamic:list")
	@RequestMapping(value = {"list", ""})
	public String list(Dynamic dynamic, HttpServletRequest request, HttpServletResponse response, Model model) {
		dynamic.setExcludeTopic(true);
		dynamic.setMemberId(officialOpenid);
		Page<Dynamic> page = dynamicService.findPage(new Page<Dynamic>(request, response), dynamic);
		model.addAttribute("page", page);
		return "modules/sc/officialDynamicList";
	}

	/**
	 * 查看，增加，编辑内容表单页面
	 */
	@RequiresPermissions(value={"sc:dynamic:view","sc:dynamic:add","sc:dynamic:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Dynamic dynamic, Model model) {
		model.addAttribute("dynamic", dynamic);
		return "modules/sc/officialForm";
	}

	/**
	 * 保存内容
	 */
	@RequiresPermissions(value={"sc:dynamic:add","sc:dynamic:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(Dynamic dynamic, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, dynamic)){
			return form(dynamic, model);
		}
		if(!dynamic.getIsNewRecord()){//编辑表单保存
			Dynamic t = dynamicService.get(dynamic.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(dynamic, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			dynamicService.save(t);//保存
		}else{//新增表单保存
			dynamicService.save(dynamic);//保存
		}
		addMessage(redirectAttributes, "保存内容成功");
		return redirect("/sc/official/?repage");
	}
	/**
	 * 删除内容
	 */
	@RequiresPermissions("sc:dynamic:del")
	@RequestMapping(value = "delete")
	public String delete(Dynamic dynamic, RedirectAttributes redirectAttributes) {
		Dynamic forwardSearch = new Dynamic();
		forwardSearch.setOriginalId(dynamic.getId());
		List<Dynamic> forwardList = dynamicService.findList(forwardSearch);
		for(Dynamic forward:forwardList){
			DynamicOperate operateSearch = new DynamicOperate();//评论
			operateSearch.setDynamicId(forward.getId());
			List<DynamicOperate> operateList = dynamicOperateService.findList(operateSearch);
			for(DynamicOperate operate:operateList){
				dynamicOperateService.delete(operate);
			}
			dynamicService.delete(forward);//转发的动态
		}
		DynamicOperate operateSearch = new DynamicOperate();//评论
		operateSearch.setDynamicId(dynamic.getId());
		List<DynamicOperate> operateList = dynamicOperateService.findList(operateSearch);
		for(DynamicOperate operate:operateList){
			dynamicOperateService.delete(operate);
		}

		Message msgSearh = new Message();
		msgSearh.setDynamicId(dynamic.getId());
		List<Message> messageList = messageService.findList(msgSearh);
		for(Message message:messageList){
			messageService.delete(message);
		}


		dynamicService.delete(dynamic);
		addMessage(redirectAttributes, "删除内容成功");
		return redirect("/sc/official/?repage");
	}
	
	/**
	 * 批量删除内容
	 */
	@RequiresPermissions("sc:dynamic:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			dynamicService.delete(dynamicService.get(id));
		}
		addMessage(redirectAttributes, "删除内容成功");
		return redirect("/sc/official/?repage");
	}

	/**
	 * 官方发布
	 */
	@RequiresPermissions(value={"sc:dynamic:view","sc:dynamic:add","sc:dynamic:edit"},logical=Logical.OR)
	@RequestMapping(value = "pubForm")
	public String pubForm(Dynamic dynamic, Model model) {
		model.addAttribute("dynamic", dynamic);
		return "modules/sc/officialDynamicPub";
	}
	@Autowired
	private MemberService memberService;

	/**
	 * 官方发布
	 */
	@RequiresPermissions(value={"sc:dynamic:add","sc:dynamic:edit"},logical=Logical.OR)
	@RequestMapping(value = "pub")
	public String pub(Dynamic dynamic, Model model, RedirectAttributes redirectAttributes) throws Exception{
		dynamic.setMemberId(officialOpenid);
		dynamic.setType(DynamicType.CNT);

		dynamic.setStatus(DynamicStatus.PASS);
		dynamic.setAuditWay(AuditWay.AUTO);
		String[] pics=null;
		if(StringUtils.isNotBlank(dynamic.getPics())){
			pics = dynamic.getPics().split("\\|");
		}
		List<String> topicNamesList = new ArrayList<String>();
		List<String> atMemberIdsList = new ArrayList<String>();
		String content = dynamic.getContent();
		final Pattern TAG_PATTERN = Pattern.compile("#([^\\#|.]+)#");//处理话题
		Matcher m = TAG_PATTERN.matcher(content);
		while (m.find()) {
			String tagNameMatch = m.group();
			tagNameMatch = tagNameMatch.replaceAll("#","");
			topicNamesList.add(tagNameMatch);
			System.out.println(tagNameMatch);
		}
		final Pattern AT_PATTERN = Pattern.compile("@[\\u4e00-\\u9fa5\\w\\-]+");//处理@
		m = AT_PATTERN.matcher(content);
		while (m.find()) {
			String atUserName = m.group();
			atUserName = atUserName.replaceAll("@","");
			Member member = new Member();
			member.setNickName(atUserName);
			List<Member> members = memberService.findList(member);
			if(members!=null&&members.size()>0){
				atMemberIdsList.add(member.getId());
			}
			System.out.println(atUserName);

		}
		String[] topicNamesArr = new String[topicNamesList.size()];
		for(int i=0;i<topicNamesList.size();i++){
			topicNamesArr[i] = topicNamesList.get(i);
		}
		String[] atMemberIdsArr = new String[atMemberIdsList.size()];
		for(int i=0;i<atMemberIdsList.size();i++){
			atMemberIdsArr[i] = atMemberIdsList.get(i);
		}

		dynamicService.createDynamic(dynamic, pics, officialOpenid,atMemberIdsArr,topicNamesArr);//保存
		addMessage(redirectAttributes, "发布官方动态成功");
		return redirect("/sc/official/?repage");
	}


}