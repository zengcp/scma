package cn.gzjp.modules.sc.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.util.List;

import cn.gzjp.common.constant.AuditWay;
import cn.gzjp.modules.sc.entity.DynamicOperate;
import cn.gzjp.modules.sc.entity.MemberFollow;
import cn.gzjp.modules.sc.entity.Message;
import cn.gzjp.modules.sc.service.DynamicOperateService;
import cn.gzjp.modules.sc.service.MemberFollowService;
import cn.gzjp.modules.sc.service.MessageService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import cn.gzjp.common.utils.DateUtils;
import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.utils.excel.ExportExcel;
import cn.gzjp.common.utils.excel.ImportExcel;
import cn.gzjp.modules.sc.entity.Dynamic;
import cn.gzjp.modules.sc.service.DynamicService;

/**
 * 动态管理Controller
 * @author zengcp
 * @version 2018-03-09
 */
@Controller
@RequestMapping(value = "${adminPath}/sc/dynamic")
public class DynamicController extends BaseController {

	@Autowired
	private DynamicService dynamicService;
	@Autowired
	private DynamicOperateService dynamicOperateService;
	@Autowired
	private MessageService messageService;
	@Autowired
	private MemberFollowService memberFollowService;
	
	
	/**
	 * 动态信息列表页面
	 */
	@RequiresPermissions("sc:dynamic:list")
	@RequestMapping(value = {"list", ""})
	public String list(Dynamic dynamic, HttpServletRequest request, HttpServletResponse response, Model model) {
		dynamic.setExcludeTopic(true);
		Page<Dynamic> page = dynamicService.findPage(new Page<Dynamic>(request, response), dynamic);
		model.addAttribute("page", page);
		return "modules/sc/dynamicList";
	}

	/**
	 * 查看，增加，编辑动态信息表单页面
	 */
	@RequiresPermissions(value={"sc:dynamic:view","sc:dynamic:add","sc:dynamic:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Dynamic dynamic, Model model) {
		model.addAttribute("dynamic", dynamic);
		return "modules/sc/dynamicForm";
	}

	/**
	 * 保存动态信息
	 */
	@RequiresPermissions(value={"sc:dynamic:add","sc:dynamic:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(Dynamic dynamic, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, dynamic)){
			return form(dynamic, model);
		}
		if(!dynamic.getIsNewRecord()){//编辑表单保存
			Dynamic t = dynamicService.get(dynamic.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(dynamic, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			dynamicService.save(t);//保存
		}else{//新增表单保存
			dynamicService.save(dynamic);//保存
		}
		addMessage(redirectAttributes, "保存动态信息成功");
		return redirect("/sc/dynamic/?repage");
	}
	
	/**
	 * 删除动态信息
	 */
	@RequiresPermissions("sc:dynamic:del")
	@RequestMapping(value = "delete")
	public String delete(Dynamic dynamic, RedirectAttributes redirectAttributes) {
		Dynamic forwardSearch = new Dynamic();
		forwardSearch.setOriginalId(dynamic.getId());
		List<Dynamic> forwardList = dynamicService.findList(forwardSearch);
		for(Dynamic forward:forwardList){
			DynamicOperate operateSearch = new DynamicOperate();//评论
			operateSearch.setDynamicId(forward.getId());
			List<DynamicOperate> operateList = dynamicOperateService.findList(operateSearch);
			for(DynamicOperate operate:operateList){
				dynamicOperateService.delete(operate);
			}
			dynamicService.delete(forward);//抓发
		}
		DynamicOperate operateSearch = new DynamicOperate();//评论
		operateSearch.setDynamicId(dynamic.getId());
		List<DynamicOperate> operateList = dynamicOperateService.findList(operateSearch);
		for(DynamicOperate operate:operateList){
			dynamicOperateService.delete(operate);
		}
		Message msgSearh = new Message();
		msgSearh.setDynamicId(dynamic.getId());
		List<Message> messageList = messageService.findList(msgSearh);
		for(Message message:messageList){
			messageService.delete(message);
		}

		MemberFollow followSearch = new MemberFollow();
		followSearch.setFollowId(dynamic.getId());
		List<MemberFollow> followList = memberFollowService.findList(followSearch);
		for(MemberFollow follow:followList){
			memberFollowService.delete(follow);
		}

		dynamicService.delete(dynamic);
		addMessage(redirectAttributes, "删除动态信息成功");
		return redirect("/sc/dynamic/");
	}
	
	/**
	 * 批量删除动态信息
	 */
	@RequiresPermissions("sc:dynamic:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			dynamicService.delete(dynamicService.get(id));
		}
		addMessage(redirectAttributes, "删除动态信息成功");
		return redirect("/sc/dynamic/");
	}

	/**
	 * 审核
	 */
	@RequiresPermissions("sc:dynamic:edit")
	@RequestMapping(value = "audit")
	public String audit(Dynamic dynamic, String status, RedirectAttributes redirectAttributes) {
		dynamic.setStatus(status);
		dynamic.setAuditWay(AuditWay.MAN);
		dynamicService.save(dynamic);
		if("1".equals(status)){//不通过  转发和评论也删除
			Dynamic forwardSearch = new Dynamic();
			forwardSearch.setOriginalId(dynamic.getId());
			List<Dynamic> forwardList = dynamicService.findList(forwardSearch);
			for(Dynamic forward:forwardList){
				forward.setStatus("1");
				dynamicService.save(forward);//抓发

				DynamicOperate operateSearch = new DynamicOperate();//评论
				operateSearch.setDynamicId(forward.getId());
				List<DynamicOperate> operateList = dynamicOperateService.findList(operateSearch);
				for(DynamicOperate operate:operateList){
					operate.setStatus("1");
					dynamicOperateService.save(operate);
				}
			}
			DynamicOperate operateSearch = new DynamicOperate();//转发
			operateSearch.setDynamicId(dynamic.getId());
			List<DynamicOperate> operateList = dynamicOperateService.findList(operateSearch);
			for(DynamicOperate operate:operateList){
				operate.setStatus("1");
				dynamicOperateService.save(operate);
			}
			Message msgSearh = new Message();
			msgSearh.setDynamicId(dynamic.getId());
			List<Message> messageList = messageService.findList(msgSearh);
			for(Message message:messageList){
				message.setStatus("1");
				messageService.save(message);
			}
		}
		MemberFollow followSearch = new MemberFollow();
		followSearch.setFollowId(dynamic.getId());
		List<MemberFollow> followList = memberFollowService.findList(followSearch);
		for(MemberFollow follow:followList){
			memberFollowService.delete(follow);
		}

		addMessage(redirectAttributes, "操作成功");
		return redirect("/sc/dynamic/");
	}



	@ModelAttribute
	public Dynamic get(@RequestParam(required=false) String id) {
		Dynamic entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = dynamicService.get(id,"");
		}
		if (entity == null){
			entity = new Dynamic();
		}
		return entity;
	}
	

}