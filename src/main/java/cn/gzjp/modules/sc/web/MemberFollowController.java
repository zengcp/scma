package cn.gzjp.modules.sc.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.util.List;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import cn.gzjp.common.utils.DateUtils;
import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.utils.excel.ExportExcel;
import cn.gzjp.common.utils.excel.ImportExcel;
import cn.gzjp.modules.sc.entity.MemberFollow;
import cn.gzjp.modules.sc.service.MemberFollowService;

/**
 * 会员关注Controller
 * @author zengcp
 * @version 2018-03-07
 */
@Controller
@RequestMapping(value = "${adminPath}/sc/memberFollow")
public class MemberFollowController extends BaseController {

	@Autowired
	private MemberFollowService memberFollowService;
	
	
	/**
	 * 会员关注列表页面
	 */
	@RequiresPermissions("sc:memberFollow:list")
	@RequestMapping(value = {"list", ""})
	public String list(MemberFollow memberFollow, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MemberFollow> page = memberFollowService.findPage(new Page<MemberFollow>(request, response), memberFollow); 
		model.addAttribute("page", page);
		return "modules/sc/memberFollowList";
	}

	/**
	 * 查看，增加，编辑会员关注表单页面
	 */
	@RequiresPermissions(value={"sc:memberFollow:view","sc:memberFollow:add","sc:memberFollow:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MemberFollow memberFollow, Model model) {
		model.addAttribute("memberFollow", memberFollow);
		return "modules/sc/memberFollowForm";
	}

	/**
	 * 保存会员关注
	 */
	@RequiresPermissions(value={"sc:memberFollow:add","sc:memberFollow:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(MemberFollow memberFollow, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, memberFollow)){
			return form(memberFollow, model);
		}
		if(!memberFollow.getIsNewRecord()){//编辑表单保存
			MemberFollow t = memberFollowService.get(memberFollow.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(memberFollow, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			memberFollowService.save(t);//保存
		}else{//新增表单保存
			memberFollowService.save(memberFollow);//保存
		}
		addMessage(redirectAttributes, "保存会员关注成功");
		return redirect("/sc/memberFollow/?repage");
	}
	
	/**
	 * 删除会员关注
	 */
	@RequiresPermissions("sc:memberFollow:del")
	@RequestMapping(value = "delete")
	public String delete(MemberFollow memberFollow, RedirectAttributes redirectAttributes) {
		memberFollowService.delete(memberFollow);
		addMessage(redirectAttributes, "删除会员关注成功");
		return redirect("/sc/memberFollow/?repage");
	}
	
	/**
	 * 批量删除会员关注
	 */
	@RequiresPermissions("sc:memberFollow:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			memberFollowService.delete(memberFollowService.get(id));
		}
		addMessage(redirectAttributes, "删除会员关注成功");
		return redirect("/sc/memberFollow/?repage");
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("sc:memberFollow:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(MemberFollow memberFollow, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "会员关注"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MemberFollow> page = memberFollowService.findPage(new Page<MemberFollow>(request, response, -1), memberFollow);
    		new ExportExcel("会员关注", MemberFollow.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出会员关注记录失败！失败信息："+e.getMessage());
		}
		return redirect("/sc/memberFollow/?repage");
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("sc:memberFollow:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MemberFollow> list = ei.getDataList(MemberFollow.class);
			for (MemberFollow memberFollow : list){
				try{
					memberFollowService.save(memberFollow);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条会员关注记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条会员关注记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入会员关注失败！失败信息："+e.getMessage());
		}
		return redirect("/sc/memberFollow/?repage");
    }
	
	/**
	 * 下载导入会员关注数据模板
	 */
	@RequiresPermissions("sc:memberFollow:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "会员关注数据导入模板.xlsx";
    		List<MemberFollow> list = Lists.newArrayList(); 
    		new ExportExcel("会员关注数据", MemberFollow.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return redirect("/sc/memberFollow/?repage");
    }
	@ModelAttribute
	public MemberFollow get(@RequestParam(required=false) String id) {
		MemberFollow entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = memberFollowService.get(id);
		}
		if (entity == null){
			entity = new MemberFollow();
		}
		return entity;
	}
	

}