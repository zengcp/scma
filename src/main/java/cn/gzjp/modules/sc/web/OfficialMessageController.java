package cn.gzjp.modules.sc.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.utils.DateUtils;
import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.utils.excel.ExportExcel;
import cn.gzjp.common.utils.excel.ImportExcel;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.sc.entity.DynamicOperate;
import cn.gzjp.modules.sc.entity.Message;
import cn.gzjp.modules.sc.service.DynamicOperateService;
import cn.gzjp.modules.sc.service.MessageService;
import com.google.common.collect.Lists;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.util.List;

/**
 * 官方消息Controller
 * @author zengcp
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/sc/officialMessage")
public class OfficialMessageController extends BaseController {

	@Autowired
	private MessageService messageService;
	@Autowired
	private DynamicOperateService dynamicOperateService;

	private final static String officialOpenid = Global.getOfficialOpenid();
	
	/**
	 * 消息列表页面
	 */
	@RequiresPermissions("sc:message:list")
	@RequestMapping(value = {"list", ""})
	public String list(Message message, HttpServletRequest request, HttpServletResponse response, Model model) {
		message.setReceiverId(officialOpenid);
		Page<Message> page = messageService.findPage(new Page<Message>(request, response), message);
		model.addAttribute("page", page);
		return "modules/sc/officialMessageList";
	}

	/**
	 * 查看，增加，编辑消息表单页面
	 */
	@RequiresPermissions(value={"sc:message:view","sc:message:add","sc:message:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(String id, Model model) {
		Message message = messageService.get(id);
		model.addAttribute("message", message);
		return "modules/sc/messageForm";
	}

	/**
	 * 保存消息
	 */
	@RequiresPermissions(value={"sc:message:add","sc:message:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(Message message, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, message)){
			return form(message.getId(), model);
		}
		if(!message.getIsNewRecord()){//编辑表单保存
			Message t = messageService.get(message.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(message, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			messageService.save(t);//保存
		}else{//新增表单保存
			messageService.save(message);//保存
		}
		addMessage(redirectAttributes, "保存消息成功");
		return redirect("/sc/officialMessage/?repage");
	}
	
	/**
	 * 删除消息
	 */
	@RequiresPermissions("sc:message:del")
	@RequestMapping(value = "delete")
	public String delete(Message message, RedirectAttributes redirectAttributes) {
		messageService.delete(message);
		addMessage2(redirectAttributes, "删除消息成功");
		return redirect("/sc/officialMessage/?repage");
	}
	
	/**
	 * 批量删除消息
	 */
	@RequiresPermissions("sc:message:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			messageService.delete(messageService.get(id));
		}
		addMessage2(redirectAttributes, "删除消息成功");
		return redirect("/sc/officialMessage/?repage");
	}
	


	/**
	 * 回复界面
	 */
	@RequestMapping(value = "reply",method = RequestMethod.GET)
	public String reply(Message message, Model model) {
		message = messageService.get(message.getId());
		model.addAttribute("message", message);
		return "modules/sc/replyMessage";
	}

	/**
	 * 回复消息
	 */
	@RequestMapping(value = "reply",method = RequestMethod.POST)
	public String reply(String id,String replyContent, RedirectAttributes redirectAttributes) {
		Message message = messageService.get(id);
		message.setStatus("1");
		message.setReplyContent(replyContent);
		messageService.save(message);

		String dynamicId = message.getDynamicId();
		String[] atMemberIds  = {message.getSenderId()};
		if(StringUtils.isNotBlank(dynamicId)){
			dynamicOperateService.comment(dynamicId,replyContent,message.getReceiverId(),atMemberIds);
		}
		else{
			Message newMsg = new Message();
			newMsg.setSenderId(message.getReceiverId());
			newMsg.setReceiverId(message.getSenderId());
			newMsg.setStatus("0");
			newMsg.setContent(replyContent);
			newMsg.setType(message.getType());
			newMsg.setDynamicId(message.getDynamicId());
			messageService.save(newMsg);
		}
		addMessage2(redirectAttributes, "操作成功");
		return redirect("/sc/officialMessage/?repage");
	}
	

}