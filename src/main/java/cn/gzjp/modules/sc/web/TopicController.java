package cn.gzjp.modules.sc.web;

import cn.gzjp.common.constant.AuditWay;
import cn.gzjp.common.constant.DynamicType;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.sc.entity.Dynamic;
import cn.gzjp.modules.sc.service.DynamicService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 话题管理Controller
 * @author zengcp
 * @version 2018-03-09
 */
@Controller
@RequestMapping(value = "${adminPath}/sc/topic")
public class TopicController extends BaseController {

	@Autowired
	private DynamicService dynamicService;
	
	
	/**
	 * 动态信息列表页面
	 */
	@RequiresPermissions("sc:dynamic:list")
	@RequestMapping(value = {"list", ""})
	public String list(Dynamic dynamic, HttpServletRequest request, HttpServletResponse response, Model model) {
		dynamic.setType(DynamicType.TOPIC);
		Page<Dynamic> page = dynamicService.findPage(new Page<Dynamic>(request, response), dynamic);
		model.addAttribute("page", page);
		return "modules/sc/topicList";
	}

	/**
	 * 查看，增加，编辑动态信息表单页面
	 */
	@RequiresPermissions(value={"sc:dynamic:view","sc:dynamic:add","sc:dynamic:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Dynamic dynamic, Model model) {
		model.addAttribute("dynamic", dynamic);
		return "modules/sc/topicForm";
	}

	/**
	 * 保存动态信息
	 */
	@RequiresPermissions(value={"sc:dynamic:add","sc:dynamic:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(Dynamic dynamic, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, dynamic)){
			return form(dynamic, model);
		}
		if(!dynamic.getIsNewRecord()){//编辑表单保存
			Dynamic t = dynamicService.get(dynamic.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(dynamic, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			dynamicService.save(t);//保存
		}else{//新增表单保存
			dynamicService.save(dynamic);//保存
		}
		addMessage(redirectAttributes, "保存话题信息成功");
		return redirect("/sc/topic/?repage");
	}
	
	/**
	 * 删除动态信息
	 */
	@RequiresPermissions("sc:dynamic:del")
	@RequestMapping(value = "delete")
	public String delete(Dynamic dynamic, RedirectAttributes redirectAttributes) {
		Dynamic search = new Dynamic();
		search.setTopicIds(dynamic.getId());
		List<Dynamic> dynamicList = dynamicService.findList(search);
		for(Dynamic d:dynamicList){
			dynamicService.delete(d);
		}
		dynamicService.delete(dynamic);
		addMessage(redirectAttributes, "删除话题信息成功");
		return redirect("/sc/topic/?repage");
	}
	
	/**
	 * 批量删除动态信息
	 */
	@RequiresPermissions("sc:dynamic:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			dynamicService.delete(dynamicService.get(id));
		}
		addMessage(redirectAttributes, "删除话题成功");
		return redirect("/sc/topic/?repage");
	}



	@ModelAttribute
	public Dynamic get(@RequestParam(required=false) String id) {
		Dynamic entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = dynamicService.get(id);
		}
		if (entity == null){
			entity = new Dynamic();
		}
		return entity;
	}
	

}