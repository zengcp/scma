package cn.gzjp.modules.sc.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gzjp.modules.sc.entity.Dynamic;
import cn.gzjp.modules.sc.entity.Message;
import cn.gzjp.modules.sc.service.MessageService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.modules.sc.entity.DynamicOperate;
import cn.gzjp.modules.sc.service.DynamicOperateService;

import java.util.List;

/**
 * 评论@回复评论Controller
 * @author zengcp
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/sc/dynamicOperate")
public class DynamicOperateController extends BaseController {

	@Autowired
	private DynamicOperateService dynamicOperateService;
	@Autowired
	private MessageService messageService;
	
	/**
	 * 回复评论列表页面
	 */
	@RequiresPermissions("sc:dynamicOperate:list")
	@RequestMapping(value = {"list", ""})
	public String list(DynamicOperate dynamicOperate, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<DynamicOperate> page = dynamicOperateService.findPage(new Page<DynamicOperate>(request, response), dynamicOperate); 
		model.addAttribute("page", page);
		return "modules/sc/dynamicOperateList";
	}

	/**
	 * 查看，增加，编辑回复评论表单页面
	 */
	@RequiresPermissions(value={"sc:dynamicOperate:view","sc:dynamicOperate:add","sc:dynamicOperate:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(DynamicOperate dynamicOperate, Model model) {
		model.addAttribute("dynamicOperate", dynamicOperate);
		return "modules/sc/replyComment";
	}

	/**
	 * 保存回复评论
	 */
	@RequiresPermissions(value={"sc:dynamicOperate:add","sc:dynamicOperate:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(DynamicOperate dynamicOperate, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, dynamicOperate)){
			return form(dynamicOperate, model);
		}
		if(!dynamicOperate.getIsNewRecord()){//编辑表单保存
			DynamicOperate t = dynamicOperateService.get(dynamicOperate.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(dynamicOperate, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			dynamicOperateService.save(t);//保存
		}else{//新增表单保存
			dynamicOperateService.save(dynamicOperate);//保存
		}
		addMessage(redirectAttributes, "保存回复评论成功");
		return redirect("/sc/dynamicOperate/?repage");
	}
	
	/**
	 * 删除回复评论
	 */
	@RequiresPermissions("sc:dynamicOperate:del")
	@RequestMapping(value = "delete")
	public String delete(DynamicOperate dynamicOperate, RedirectAttributes redirectAttributes) {
		dynamicOperateService.delete(dynamicOperate);
		Message msgSearh = new Message();
		msgSearh.setOperateId(dynamicOperate.getId());
		List<Message> messageList = messageService.findList(msgSearh);
		for(Message message:messageList){
			messageService.delete(message);
		}
		addMessage(redirectAttributes, "删除回复评论成功");
		return redirect("/sc/dynamicOperate/?repage");
	}
	
	/**
	 * 批量删除回复评论
	 */
	@RequiresPermissions("sc:dynamicOperate:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			dynamicOperateService.delete(dynamicOperateService.get(id));
		}
		addMessage(redirectAttributes, "删除回复评论成功");
		return redirect("/sc/dynamicOperate/?repage");
	}

	/**
	 * 审核
	 */
	@RequiresPermissions("sc:dynamic:edit")
	@RequestMapping(value = "audit")
	public String audit(DynamicOperate dynamicOperate, String status, RedirectAttributes redirectAttributes) {
		dynamicOperate.setStatus(status);
		//dynamicOperate.setAuditWay(AuditWay.MAN);
		dynamicOperateService.save(dynamicOperate);

		if("1".equals(status)){//不通过  转发和评论也删除
			Message msgSearh = new Message();
			msgSearh.setOperateId(dynamicOperate.getId());
			List<Message> messageList = messageService.findList(msgSearh);
			for(Message message:messageList){
				message.setStatus("1");
				messageService.save(message);
			}
		}
		addMessage(redirectAttributes, "操作成功");
		return redirect("/sc/dynamicOperate/?repage");
	}
	@ModelAttribute
	public DynamicOperate get(@RequestParam(required=false) String id) {
		DynamicOperate entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = dynamicOperateService.get(id);
		}
		if (entity == null){
			entity = new DynamicOperate();
		}
		return entity;
	}

	/**
	 * 回复界面
	 */
	@RequestMapping(value = "reply",method = RequestMethod.GET)
	public String reply(DynamicOperate dynamicOperate, Model model) {
		model.addAttribute("dynamicOperate", dynamicOperate);
		return "modules/sc/replyComment";
	}

	/**
	 * 回复评论
	 */
	@RequestMapping(value = "reply",method = RequestMethod.POST)
	public String reply(DynamicOperate dynamicOperate, RedirectAttributes redirectAttributes) {
		dynamicOperateService.save(dynamicOperate);
		addMessage(redirectAttributes, "操作成功");
		return redirect("/sc/dynamicOperate/?repage");
	}

}