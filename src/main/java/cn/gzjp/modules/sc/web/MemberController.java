package cn.gzjp.modules.sc.web;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cn.gzjp.common.config.ConfigUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import cn.gzjp.common.utils.DateUtils;
import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.utils.excel.ExportExcel;
import cn.gzjp.common.utils.excel.ImportExcel;
import cn.gzjp.modules.sc.entity.Member;
import cn.gzjp.modules.sc.service.MemberService;

/**
 * 会员管理Controller
 * @author zengcp
 * @version 2018-03-04
 */
@Controller
@RequestMapping(value = "${adminPath}/sc/member")
public class MemberController extends BaseController {

	@Autowired
	private MemberService memberService;
	
	
	/**
	 * 会员列表页面
	 */
	@RequiresPermissions("sc:member:list")
	@RequestMapping(value = {"list", ""})
	public String list(Member member, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Member> page = memberService.findPage(new Page<Member>(request, response), member); 
		model.addAttribute("page", page);
		return "modules/sc/memberList";
	}

	/**
	 * 查看，增加，编辑会员表单页面
	 */
	@RequiresPermissions(value={"sc:member:view","sc:member:add","sc:member:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Member member, Model model) {
		model.addAttribute("member", member);
		return "modules/sc/memberForm";
	}

	/**
	 * 保存会员
	 */
	@RequiresPermissions(value={"sc:member:add","sc:member:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(Member member, HttpServletRequest request, HttpServletResponse response, Model model,MultipartFile file, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, member)){
			return form(member, model);
		}
		if(!member.getIsNewRecord()){//编辑表单保存
			Member t = memberService.get(member.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(member, t);//将编辑表单中的非NULL值覆盖数据库记录中的值

			if(file!=null){
				// 应用目录路径
				ServletContext sc = request.getSession().getServletContext();
				String curWebAppRoot = sc.getRealPath("/userfiles");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				String tempUploadName = sdf.format(new Date());
				// 保存文件的路径
				String outPutFilePath = curWebAppRoot + File.separator // + "temp" +
						+ "_" + tempUploadName;
				if (!file.isEmpty()) {
					String extension = FilenameUtils.getExtension(file
							.getOriginalFilename());
					try {
						File outPutFile = new File(outPutFilePath + "." + extension);
						FileUtils.writeByteArrayToFile(outPutFile, file.getBytes());
						String photo = "/userfiles/" + "_" + tempUploadName + "." + extension;
						String photoUrl = ConfigUtils.getProperty("photoUrl");
						photoUrl =  photoUrl + photo;
						photoUrl = photoUrl.replaceAll("scma/scma","scma");
						t.setAvatarUrl(photoUrl);
					} catch (IOException e) {
					}
				}
			}
			memberService.save(t);//保存

		}else{//新增表单保存
			memberService.save(member);//保存
		}
		addMessage(redirectAttributes, "保存会员成功");
		return redirect("/sc/member/?repage");
	}
	
	/**
	 * 删除会员
	 */
	@RequiresPermissions("sc:member:del")
	@RequestMapping(value = "delete")
	public String delete(Member member, RedirectAttributes redirectAttributes) {
		memberService.delete(member);
		addMessage(redirectAttributes, "删除会员成功");
		return redirect("/sc/member/?repage");
	}
	
	/**
	 * 批量删除会员
	 */
	@RequiresPermissions("sc:member:del")
	@RequestMapping(value = "deleteAll")
	public String deleteAll(String ids, RedirectAttributes redirectAttributes) {
		String idArray[] =ids.split(",");
		for(String id : idArray){
			memberService.delete(memberService.get(id));
		}
		addMessage(redirectAttributes, "删除会员成功");
		return redirect("/sc/member/?repage");
	}
	
	/**
	 * 导出excel文件
	 */
	@RequiresPermissions("sc:member:export")
    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(Member member, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "会员"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Member> page = memberService.findPage(new Page<Member>(request, response, -1), member);
    		new ExportExcel("会员", Member.class).setDataList(page.getList()).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出会员记录失败！失败信息："+e.getMessage());
		}
		return redirect("/sc/member/?repage");
    }

	/**
	 * 导入Excel数据

	 */
	@RequiresPermissions("sc:member:import")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Member> list = ei.getDataList(Member.class);
			for (Member member : list){
				try{
					memberService.save(member);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条会员记录。");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条会员记录"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入会员失败！失败信息："+e.getMessage());
		}
		return redirect("/sc/member/?repage");
    }
	
	/**
	 * 下载导入会员数据模板
	 */
	@RequiresPermissions("sc:member:import")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "会员数据导入模板.xlsx";
    		List<Member> list = Lists.newArrayList(); 
    		new ExportExcel("会员数据", Member.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return redirect("/sc/member/?repage");
    }
	@ModelAttribute
	public Member get(@RequestParam(required=false) String id) {
		Member entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = memberService.get(id);
		}
		if (entity == null){
			entity = new Member();
		}
		return entity;
	}
	

}