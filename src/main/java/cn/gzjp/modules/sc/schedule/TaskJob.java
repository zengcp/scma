package cn.gzjp.modules.sc.schedule;

import cn.gzjp.modules.sc.entity.Dynamic;
import cn.gzjp.modules.sc.entity.DynamicMedia;
import cn.gzjp.modules.sc.entity.Member;
import cn.gzjp.modules.sc.entity.MemberFollow;
import cn.gzjp.modules.sc.service.DynamicMediaService;
import cn.gzjp.modules.sc.service.DynamicService;
import cn.gzjp.modules.sc.service.MemberFollowService;
import cn.gzjp.modules.sc.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 定时任务
 * 
 * @author zengcp
 *
 */
@Service
@Lazy(false)
public class TaskJob {
	
	public static Logger log = LoggerFactory.getLogger("jobLog");
	@Autowired
	private DynamicMediaService contentMediaService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private DynamicService dynamicService;
	@Autowired
	private MemberFollowService memberFollowService;
	
	@Scheduled(cron = "0 0/30 * * * ?")
	public void synchImgcensor() {
		log.info("图片定时审核开始");
//		DynamicMedia search = new DynamicMedia();
//		search.setUncensor("yes");
//		List<DynamicMedia> contentMediaList = contentMediaService.findList(search);
//		for (DynamicMedia media : contentMediaList) {
//			try {
//				contentMediaService.censorImg(media);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				//e.printStackTrace();
//			}
//		}
//		log.info("图片定时审核结束");
//
//		log.info("更新发表动态数");
//
//
		List<Member> memberList = memberService.findList(new Member());
		for(Member member:memberList){
			Dynamic dySearch = new Dynamic();
			dySearch.setMemberId(member.getId());
			dySearch.setStatus("0");
			List<Dynamic> dynamicList = dynamicService.findList(dySearch);
			int count = 0;
			for(Dynamic dynamic:dynamicList){
				if(!"1".equals(dynamic.getType())){
					count++ ;
				}
			}
			member.setDynamicCount(count);
			MemberFollow followSearch = new MemberFollow();
			followSearch.setMemberId(member.getId());
			List<MemberFollow> followList = memberFollowService.findList(followSearch);
			member.setFollowCount(followList.size());
			memberService.save(member);
		}
		log.info("更新发表动态数结束");
	}
	
}
