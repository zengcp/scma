package cn.gzjp.modules.sc.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.sc.entity.Dynamic;

/**
 * 动态管理DAO接口
 * @author zengcp
 * @version 2018-03-09
 */
@MyBatisDao
public interface DynamicDao extends CrudDao<Dynamic> {
	
}