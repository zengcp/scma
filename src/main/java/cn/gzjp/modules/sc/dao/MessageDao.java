package cn.gzjp.modules.sc.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.sc.entity.Message;

/**
 * 消息DAO接口
 * @author zengcp
 * @version 2018-03-11
 */
@MyBatisDao
public interface MessageDao extends CrudDao<Message> {

    long findMyCount(String receiverId);
	
}