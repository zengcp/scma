package cn.gzjp.modules.sc.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.sc.entity.Member;

/**
 * 会员管理DAO接口
 * @author zengcp
 * @version 2018-03-04
 */
@MyBatisDao
public interface MemberDao extends CrudDao<Member> {
	
}