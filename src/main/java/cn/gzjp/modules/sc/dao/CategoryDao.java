package cn.gzjp.modules.sc.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.sc.entity.Category;

/**
 * 内容分类DAO接口
 * @author zengcp
 * @version 2018-03-04
 */
@MyBatisDao
public interface CategoryDao extends CrudDao<Category> {
	
}