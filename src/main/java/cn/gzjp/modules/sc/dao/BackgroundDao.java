package cn.gzjp.modules.sc.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.sc.entity.Background;

/**
 * 背景管理DAO接口
 * @author zengcp
 * @version 2018-03-04
 */
@MyBatisDao
public interface BackgroundDao extends CrudDao<Background> {
	
}