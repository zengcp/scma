package cn.gzjp.modules.sc.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.sc.entity.DynamicOperate;

/**
 * 评论@回复评论DAO接口
 * @author zengcp
 * @version 2018-03-11
 */
@MyBatisDao
public interface DynamicOperateDao extends CrudDao<DynamicOperate> {
	
}