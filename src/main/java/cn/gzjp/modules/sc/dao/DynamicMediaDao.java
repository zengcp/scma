package cn.gzjp.modules.sc.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.sc.entity.DynamicMedia;

/**
 * 图片视频DAO接口
 * @author zengcp
 * @version 2018-03-06
 */
@MyBatisDao
public interface DynamicMediaDao extends CrudDao<DynamicMedia> {
	
}