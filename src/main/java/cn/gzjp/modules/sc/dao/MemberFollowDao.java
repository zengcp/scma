package cn.gzjp.modules.sc.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.sc.entity.MemberFollow;

/**
 * 会员关注DAO接口
 * @author zengcp
 * @version 2018-03-07
 */
@MyBatisDao
public interface MemberFollowDao extends CrudDao<MemberFollow> {
	
}