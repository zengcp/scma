package cn.gzjp.modules.sc.service;

import java.util.List;

import cn.gzjp.common.utils.StringUtil;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.modules.sc.entity.DynamicOperate;
import com.xiaoleilu.hutool.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.sc.entity.Message;
import cn.gzjp.modules.sc.dao.MessageDao;

/**
 * 消息Service
 * @author zengcp
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class MessageService extends CrudService<MessageDao, Message> {

	@Autowired
	private DynamicService dynamicService;

	@Autowired
	private DynamicOperateService dynamicOperateService;
	@Override
	public Message get(String id) {
		Message message = super.get(id);
		if(StringUtils.isNotBlank(message.getDynamicId())){
			message.setDynamic(dynamicService.get(message.getDynamicId()));
		}
		return message;
	}
	@Override
	public List<Message> findList(Message message) {
		return super.findList(message);
	}
	@Override
	public Page<Message> findPage(Page<Message> page, Message message) {
		return super.findPage(page, message);
	}
	@Override
	@Transactional(readOnly = false)
	public void save(Message message) {
		super.save(message);
	}
	@Override
	@Transactional(readOnly = false)
	public void delete(Message message) {
		if(StrUtil.isNotBlank(message.getOperateId())){
			DynamicOperate dynamicOperate = dynamicOperateService.get(message.getOperateId());
			if(dynamicOperate!=null)
			dynamicOperateService.delete(dynamicOperate);
		}
		super.deleteByLogic(message);
	}


	@Transactional(readOnly = false,rollbackFor=RuntimeException.class)
	public Message reply(Message message,String originalMsgId){
		Message originalMsg = this.get(originalMsgId);
		originalMsg.setReplyContent(message.getContent());
		this.save(originalMsg);
		this.save(message);
		return  message;
	}

	public long findMyCount(String receiverId){
		return dao.findMyCount(receiverId);
	}
	
}