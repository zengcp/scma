package cn.gzjp.modules.sc.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.sc.entity.Background;
import cn.gzjp.modules.sc.dao.BackgroundDao;

/**
 * 背景管理Service
 * @author zengcp
 * @version 2018-03-04
 */
@Service
@Transactional(readOnly = true)
public class BackgroundService extends CrudService<BackgroundDao, Background> {
	@Override
	public Background get(String id) {
		return super.get(id);
	}
	@Override
	public List<Background> findList(Background background) {
		return super.findList(background);
	}
	@Override
	public Page<Background> findPage(Page<Background> page, Background background) {
		return super.findPage(page, background);
	}
	@Override
	@Transactional(readOnly = false)
	public void save(Background background) {
		super.save(background);
	}
	@Override
	@Transactional(readOnly = false)
	public void delete(Background background) {
		super.deleteByLogic(background);
	}
	
}