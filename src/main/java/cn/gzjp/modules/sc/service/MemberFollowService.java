package cn.gzjp.modules.sc.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.sc.entity.MemberFollow;
import cn.gzjp.modules.sc.dao.MemberFollowDao;

/**
 * 会员关注Service
 * @author zengcp
 * @version 2018-03-07
 */
@Service
@Transactional(readOnly = true)
public class MemberFollowService extends CrudService<MemberFollowDao, MemberFollow> {
	@Override
	public MemberFollow get(String id) {
		return super.get(id);
	}
	@Override
	public List<MemberFollow> findList(MemberFollow memberFollow) {
		return super.findList(memberFollow);
	}
	@Override
	public Page<MemberFollow> findPage(Page<MemberFollow> page, MemberFollow memberFollow) {
		return super.findPage(page, memberFollow);
	}
	@Override
	@Transactional(readOnly = false)
	public void save(MemberFollow memberFollow) {
		super.save(memberFollow);
	}
	@Override
	@Transactional(readOnly = false)
	public void delete(MemberFollow memberFollow) {
		super.deleteByLogic(memberFollow);
	}
	
}