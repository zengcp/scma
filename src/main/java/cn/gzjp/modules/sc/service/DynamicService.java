package cn.gzjp.modules.sc.service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.gzjp.common.constant.AuditWay;
import cn.gzjp.common.constant.DynamicStatus;
import cn.gzjp.common.constant.DynamicType;
import cn.gzjp.common.utils.FlowIdUtil;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.modules.sc.entity.*;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.sc.dao.DynamicDao;

/**
 * 动态管理Service
 *
 * @author zengcp
 * @version 2018-03-09
 */
@Service
@Transactional(readOnly = true)
public class DynamicService extends CrudService<DynamicDao, Dynamic> {

    @Autowired
    private DynamicMediaService dynamicMediaService;

    @Autowired
    private DynamicOperateService dynamicOperateService;
    @Autowired
    private MemberFollowService memberFollowService;

    @Autowired
    private MemberService memberService;
    @Autowired
    private MessageService messageService;

    @Transactional(readOnly = false)
    public Dynamic get(String id, String myopenid) {
        Dynamic dynamic = super.get(id);
        DynamicMedia search = new DynamicMedia();
        search.setDynamicId(id);
        dynamic.setMediaList(dynamicMediaService.findList(search));
        DynamicOperate searchComment = new DynamicOperate();
        searchComment.setStatus("0");//通过
        searchComment.setDynamicId(id);
        List<DynamicOperate> commentList = dynamicOperateService.findList(searchComment);
        dynamic.setCommentList(commentList);
        dynamic.setOriginal(true);
        if (StringUtils.isNotBlank(dynamic.getOriginalId())) {
            Dynamic originalDynamic = this.getOriginDynamic(dynamic.getOriginalId());
            dynamic.setOriginalDynamic(originalDynamic);
            dynamic.setOriginal(false);
        }
        dynamic.setCollect(false);
        MemberFollow memberFollow = new MemberFollow();
        memberFollow.setMemberId(myopenid);
        memberFollow.setFollowId(id);
        List<MemberFollow> list = memberFollowService.findList(memberFollow);
        if (list != null && list.size() > 0) {
            dynamic.setCollect(true);
        }

        Dynamic forwardSearch = new Dynamic();
        forwardSearch.setType("3");//转发
        forwardSearch.setOriginalId(id);
        forwardSearch.setStatus("0");
        List<Dynamic> forwardList = this.findList(forwardSearch);
        dynamic.setForwardList(forwardList);
        if(dynamic.getCommentCount()!=commentList.size()||dynamic.getForwardCount()!=forwardList.size()){
            dynamic.setCommentCount(commentList.size());
            dynamic.setForwardCount(forwardList.size());
            this.save(dynamic);
        }

        return dynamic;
    }

    private Dynamic getOriginDynamic(String originId){
        Dynamic dynamic = super.get(originId);
        DynamicMedia search = new DynamicMedia();
        search.setDynamicId(originId);
        dynamic.setMediaList(dynamicMediaService.findList(search));
        return dynamic;
    }

    @Override
    public List<Dynamic> findList(Dynamic dynamic) {
        return super.findList(dynamic);
    }
    @Override
    public Page<Dynamic> findPage(Page<Dynamic> page, Dynamic dynamic) {
        return super.findPage(page, dynamic);
    }
    @Override
    @Transactional(readOnly = false)
    public void save(Dynamic dynamic) {
        super.save(dynamic);
    }
    @Override
    @Transactional(readOnly = false)
    public void delete(Dynamic dynamic) {
        super.deleteByLogic(dynamic);
    }


    static final Pattern p = Pattern.compile("\\s*|\t|\r|\n");

    /**
     * 发布动态
     * @param dynamic
     * @param filePaths
     * @param openid
     * @param atMemberIds
     * @param topicNames
     * @return
     */
    @Transactional(readOnly = false,rollbackFor=RuntimeException.class)
    public Dynamic createDynamic(Dynamic dynamic, String[] filePaths, String openid,String[] atMemberIds,String[] topicNames) {
        String content = dynamic.getContent();
        if (content != null) {
            Matcher m = p.matcher(content);
            content = m.replaceAll("");
            dynamic.setContent(content);
        }
        String tIds = "";
        String tNames="";

        if(topicNames != null && ArrayUtils.isNotEmpty(topicNames)){//处理话题
            for (String topicName : topicNames) {
                if(StringUtils.isNotBlank(topicName)){
                    topicName = topicName.trim();
                    Dynamic search = new Dynamic();
                    search.setType(DynamicType.TOPIC);
                    search.setExcludeTopic(false);
                    search.setTopicNames(topicName);
                    List<Dynamic> topicList = this.findList(search);
                    Dynamic topic =null;
                    if(topicList!=null&&topicList.size()>0){
                        topic = topicList.get(0);
                        topic.setCommentCount(topic.getCommentCount()+1);
                        this.save(topic);
                    }else{
                        topic = new Dynamic();
                        topic.setTopicNames(topicName);
                        topic.setType(DynamicType.TOPIC);
                        topic.setStatus(DynamicStatus.PASS);
                        topic.setAuditWay(AuditWay.AUTO);
                        topic.setMemberId(dynamic.getMemberId());
                        String topicId=  FlowIdUtil.getInstance().getFlowId();
                        topic.setCommentCount(1);
                        this.insert(topic, topicId);//创建话题

                    }
                    tIds += topic.getId()+"|";
                    tNames += topicName+"|";
                }
            }
            if(tIds.length()>0){
                tIds =  tIds.substring(0,tIds.length()-1);
                dynamic.setTopicIds(tIds);
            }
            if(tNames.length()>0){
                tNames =  tNames.substring(0,tNames.length()-1);
                dynamic.setTopicNames(tNames);
            }
        }
        this.insert(dynamic, FlowIdUtil.getInstance().getFlowId());
        if (filePaths != null && ArrayUtils.isNotEmpty(filePaths)) {//处理图片
            int sort = 1;
            for (String filePath : filePaths) {
                if(StringUtils.isNotBlank(filePath)){
                    DynamicMedia contentMedia = new DynamicMedia();
                    contentMedia.setDynamicId(dynamic.getId());
                    contentMedia.setPath(filePath);
                    contentMedia.setSort(sort);
                    dynamicMediaService.save(contentMedia);
                    sort++;
                }
            }
        }
        String atIds = "";
        String atNames = "";
        if (atMemberIds != null && ArrayUtils.isNotEmpty(atMemberIds)) {//处理@，并发消息
            for (String atMemberId : atMemberIds) {//发消息
                if(StringUtils.isNotBlank(atMemberId)){
                    Message message = new Message();
                    message.setSenderId(openid);
                    message.setReceiverId(atMemberId);
                    message.setContent(content);
                    message.setStatus("0");
                    message.setDynamicId(dynamic.getId());
                    message.setType("2");//原创
                    messageService.save(message);
                    Member member = memberService.get(atMemberId);
                    atIds += atMemberId+"|";
                    atNames += member.getNickName()+"|";
                }
            }
            if(atIds.length()>0){
                atIds =  atIds.substring(0,atIds.length()-1);
                dynamic.setAtMeberIds(atIds);
            }
            if(atNames.length()>0){
                atNames =  atNames.substring(0,atNames.length()-1);
                dynamic.setAtMemberNames(atNames);
            }
            this.save(dynamic);
        }
        Member member = memberService.get(dynamic.getMemberId());//更新我的动态数
        member.setDynamicCount(member.getDynamicCount()+1);
        memberService.save(member);
        return dynamic;
    }

    /**
     * 转发动态
     * @param dynamic
     * @param openid
     * @param atMemberIds
     * @param topicNames
     * @return
     */
    @Transactional(readOnly = false,rollbackFor=RuntimeException.class)
    public Dynamic forwardDynamic(Dynamic dynamic, String openid,String[] atMemberIds,String[] topicNames) {
        String content = dynamic.getContent();
        if (content != null) {
            Matcher m = p.matcher(content);
            content = m.replaceAll("");
            dynamic.setContent(content);
        }
        String tIds = "";
        String tNames="";
        if(topicNames != null && ArrayUtils.isNotEmpty(topicNames)){//处理发话题
            for (String topicName : topicNames) {
                topicName = topicName.trim();
                Dynamic search = new Dynamic();
                search.setType(DynamicType.TOPIC);
                search.setExcludeTopic(false);
                search.setTopicNames(topicName);
                List<Dynamic> topicList = this.findList(search);
                Dynamic topic =null;
                if(topicList!=null&&topicList.size()>0){
                    topic = topicList.get(0);
                    topic.setCommentCount(topic.getCommentCount()+1);
                    this.save(topic);
                }else{
                    topic = new Dynamic();
                    topic.setTopicNames(topicName);
                    topic.setType(DynamicType.TOPIC);
                    topic.setStatus(DynamicStatus.PASS);
                    topic.setAuditWay(AuditWay.AUTO);
                    topic.setMemberId(dynamic.getMemberId());
                    topic.setCommentCount(1);
                    String topicId=  FlowIdUtil.getInstance().getFlowId();
                    this.insert(topic, topicId);//创建话题

                }
                tIds += topic.getId()+"|";
                tNames += topicName+"|";
            }
            tIds =  tIds.substring(0,tIds.length()-1);
            tNames =  tNames.substring(0,tNames.length()-1);
            dynamic.setTopicIds(tIds);
            dynamic.setTopicNames(tNames);
        }
        this.insert(dynamic, FlowIdUtil.getInstance().getFlowId());

        Dynamic original = this.get(dynamic.getOriginalId());
        original.setForwardCount(original.getForwardCount()+1);//转发领
        this.save(original);
        Member member = memberService.get(dynamic.getMemberId());
        member.setDynamicCount(member.getDynamicCount()+1);
        memberService.save(member);
        String atIds = "";
        String atNames = "";
        if (atMemberIds != null && ArrayUtils.isNotEmpty(atMemberIds)) {//@发发消息
            for (String atMemberId : atMemberIds) {//发消息
                Message message = new Message();
                message.setSenderId(openid);
                message.setReceiverId(atMemberId);
                message.setContent(content);
                message.setStatus("0");
                message.setDynamicId(dynamic.getId());
                message.setType("1");//原创
                messageService.save(message);
                member = memberService.get(atMemberId);
                atIds += atMemberId+"|";
                atNames += member.getNickName()+"|";
            }
            atIds =  atIds.substring(0,atIds.length()-1);
            atNames =  atNames.substring(0,atNames.length()-1);
            dynamic.setAtMeberIds(atIds);
            dynamic.setAtMemberNames(atNames);
            this.save(dynamic);
        }
        return dynamic;
    }
}