package cn.gzjp.modules.sc.service;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.WxMaInMemoryConfig;
import cn.gzjp.common.config.ConfigUtils;

public class WxService {

	public static WxMaService getWxMaService() {
		WxMaInMemoryConfig config = new WxMaInMemoryConfig();
		config.setAppid(ConfigUtils.getProperty("appid"));
		config.setSecret(ConfigUtils.getProperty("secret"));
		config.setToken(ConfigUtils.getProperty("token"));
		config.setAesKey(ConfigUtils.getProperty("aesKey"));
		config.setMsgDataFormat(ConfigUtils.getProperty("msgDataFormat"));
		WxMaService service = new WxMaServiceImpl();
		service.setWxMaConfig(config);
		return service;
	}

}
