package cn.gzjp.modules.sc.service;

import java.util.List;

import cn.gzjp.common.constant.DynamicOperateType;
import cn.gzjp.common.constant.DynamicStatus;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.modules.sc.entity.Dynamic;
import cn.gzjp.modules.sc.entity.Member;
import cn.gzjp.modules.sc.entity.Message;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.sc.entity.DynamicOperate;
import cn.gzjp.modules.sc.dao.DynamicOperateDao;

/**
 * 评论@回复评论Service
 * @author zengcp
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class DynamicOperateService extends CrudService<DynamicOperateDao, DynamicOperate> {
	@Autowired
	private DynamicService dynamicService;

	@Autowired
	private MessageService messageService;
	@Autowired
	private MemberService memberService;

	/**
	 * 评论动态
	 * @param dynamicId
	 * @param content
	 * @param openid
	 * @param atMemberIds
	 * @return
	 */
	@Transactional(readOnly = false,rollbackFor=RuntimeException.class)
	public DynamicOperate comment(String dynamicId,String content,String openid,String[] atMemberIds){
		Dynamic dynamic = dynamicService.get(dynamicId);
		dynamic.setCommentCount(dynamic.getCommentCount() + 1);
		dynamicService.save(dynamic);
		//评论操作
		DynamicOperate operate = new DynamicOperate();
		operate.setType(DynamicOperateType.COMMENT);
		operate.setContent(content);
		operate.setMemberId(openid);
		operate.setReceiverId(dynamic.getMemberId());
		operate.setStatus(DynamicStatus.PASS);
		operate.setDynamicId(dynamicId);
		this.save(operate);
		String atIds = "";
		String atNames = "";
		if (atMemberIds != null && ArrayUtils.isNotEmpty(atMemberIds)) {//@发消息
			for (String atMemberId : atMemberIds) {//发消息
				Message message = new Message();
				message.setSenderId(openid);
				message.setReceiverId(atMemberId);
				message.setContent(content);
				message.setStatus("0");
				message.setType("3");//评论
				message.setDynamicId(dynamicId);
				message.setOperateId(operate.getId());//设置评论id
				messageService.save(message);
				Member member = memberService.get(atMemberId);
				atIds += atMemberId+"|";
				atNames += member.getNickName()+"|";
			}
			if(atIds.length()>0){
				atIds =  atIds.substring(0,atIds.length()-1);
				operate.setAtMeberIds(atIds);
			}
			if(atNames.length()>0){
				atNames =  atNames.substring(0,atNames.length()-1);
				operate.setAtMemberNames(atNames);
			}
			this.save(operate);
		}
		return operate;
	}
	@Override
	public DynamicOperate get(String id) {
		DynamicOperate operate = super.get(id);
		if(StringUtils.isNotBlank(operate.getDynamicId())){
			operate.setDynamic(dynamicService.get(operate.getDynamicId()));
		}
		return operate;
	}
	@Override
	public List<DynamicOperate> findList(DynamicOperate dynamicOperate) {
		return super.findList(dynamicOperate);
	}
	@Override
	public Page<DynamicOperate> findPage(Page<DynamicOperate> page, DynamicOperate dynamicOperate) {
		return super.findPage(page, dynamicOperate);
	}
	@Override
	@Transactional(readOnly = false)
	public void save(DynamicOperate dynamicOperate) {
		super.save(dynamicOperate);
	}
	@Override
	@Transactional(readOnly = false)
	public void delete(DynamicOperate dynamicOperate) {
		super.deleteByLogic(dynamicOperate);
	}
	
}