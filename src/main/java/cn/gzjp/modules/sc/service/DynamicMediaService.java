package cn.gzjp.modules.sc.service;

import java.util.List;

import cn.gzjp.common.constant.AuditWay;
import cn.gzjp.common.constant.DynamicStatus;
import cn.gzjp.common.utils.StringUtil;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.modules.sc.entity.Dynamic;
import com.baidu.aip.imagecensor.AipImageCensor;
import com.baidu.aip.imagecensor.EImgType;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.sc.entity.DynamicMedia;
import cn.gzjp.modules.sc.dao.DynamicMediaDao;

/**
 * 图片视频Service
 * @author zengcp
 * @version 2018-03-06
 */
@Service
@Transactional(readOnly = true)
public class DynamicMediaService extends CrudService<DynamicMediaDao, DynamicMedia> {
	@Autowired
	private DynamicService dynamicService;
	@Override
	public DynamicMedia get(String id) {
		return super.get(id);
	}
	@Override
	public List<DynamicMedia> findList(DynamicMedia contentMedia) {
		return super.findList(contentMedia);
	}
	@Override
	public Page<DynamicMedia> findPage(Page<DynamicMedia> page, DynamicMedia contentMedia) {
		return super.findPage(page, contentMedia);
	}
	@Override
	@Transactional(readOnly = false)
	public void save(DynamicMedia contentMedia) {
		super.save(contentMedia);
	}
	@Override
	@Transactional(readOnly = false)
	public void delete(DynamicMedia contentMedia) {
		super.deleteByLogic(contentMedia);
	}

	public static final String APP_ID = "10576505";
	public static final String API_KEY = "EIeg8vTBY5RDof8hPynA55A5";
	public static final String SECRET_KEY = "9IbrpntjheaBjyRFw5dw5LkZOdxWDWmT";

	public static Logger log = LoggerFactory.getLogger("jobLog");
	/**
	 * 审核图片
	 * @param media
	 */
	@Transactional(readOnly = false)
	public void censorImg(DynamicMedia media) {
		AipImageCensor client = new AipImageCensor(APP_ID, API_KEY, SECRET_KEY);
		log.info("待审核图片地址="+media.getMediaFilePath());
		if(StringUtils.isNotBlank(media.getMediaFilePath())){
			JSONObject response =null;
			if(media.getMediaFilePath().startsWith("http")){
				response = client.imageCensorUserDefined(media.getMediaFilePath(), EImgType.URL, null);
			}else{
				response = client.imageCensorUserDefined(media.getMediaFilePath(), EImgType.FILE, null);
			}
			String conclusion = response.getString("conclusion");
			String msg="";
			log.info("conclusion什么结果="+conclusion);
			if("不合规".equals(conclusion)){
				JSONArray msgArray = response.getJSONArray("data");
				msg =  msgArray.getJSONObject(0).getString("msg");
				log.info("msg="+msg);
				Dynamic dynamic = dynamicService.get(media.getDynamicId());
				dynamic.setAuditWay(AuditWay.AUTO);
				dynamic.setStatus(DynamicStatus.UNPASS);
				dynamic.setRemarks(msg);
				dynamicService.save(dynamic);
			}
			media.setConclusion(conclusion);
			media.setMsg(msg);
			this.save(media);
		}


	}
	
}