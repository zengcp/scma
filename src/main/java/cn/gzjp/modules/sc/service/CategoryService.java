package cn.gzjp.modules.sc.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.sc.entity.Category;
import cn.gzjp.modules.sc.dao.CategoryDao;

/**
 * 内容分类Service
 * @author zengcp
 * @version 2018-03-04
 */
@Service
@Transactional(readOnly = true)
public class CategoryService extends CrudService<CategoryDao, Category> {
	@Override
	public Category get(String id) {
		return super.get(id);
	}
	@Override
	public List<Category> findList(Category category) {
		return super.findList(category);
	}
	@Override
	public Page<Category> findPage(Page<Category> page, Category category) {
		return super.findPage(page, category);
	}
	@Override
	@Transactional(readOnly = false)
	public void save(Category category) {
		super.save(category);
	}
	@Override
	@Transactional(readOnly = false)
	public void delete(Category category) {
		super.deleteByLogic(category);
	}
	
}