package cn.gzjp.modules.sc.service;

import java.util.List;

import cn.gzjp.modules.sc.entity.MemberFollow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.sc.entity.Member;
import cn.gzjp.modules.sc.dao.MemberDao;

/**
 * 会员管理Service
 * @author zengcp
 * @version 2018-03-04
 */
@Service
@Transactional(readOnly = true)
public class MemberService extends CrudService<MemberDao, Member> {
	@Autowired
	private MemberFollowService memberFollowService;

	@Autowired
	private MessageService messageService;

	@Override
	public Member get(String id) {
		Member member = super.get(id);
		if(member!=null){
			member.setMessageCount(messageService.findMyCount(id));
		}
		return member;

	}
	public Member get(String id,String myopenid) {
		Member member =  super.get(id);
		if(member==null){
			return new Member();
		}
		member.setFollow(false);
		MemberFollow memberFollow = new MemberFollow();
		memberFollow.setMemberId(myopenid);
		memberFollow.setFollowId(id);
		List<MemberFollow> list = memberFollowService.findList(memberFollow);
		if (list != null && list.size() > 0) {
			member.setFollow(true);
		}
		member.setMessageCount(messageService.findMyCount(id));
		return member;
	}





	@Override
	public List<Member> findList(Member member) {
		return super.findList(member);
	}
	@Override
	public Page<Member> findPage(Page<Member> page, Member member) {
		return super.findPage(page, member);
	}

	
	@Transactional(readOnly = false)
	@Override
	public void delete(Member member) {
		super.deleteByLogic(member);
	}

	@Transactional(readOnly = false)
	@Override
	public void save(Member member) {

		Member m = get(member.getId());
		if(m!=null){
			super.save(member);
		}else{
			super.insert(member, member.getId());
		}
	}

}