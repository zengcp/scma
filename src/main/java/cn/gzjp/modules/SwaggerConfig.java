package cn.gzjp.modules;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
@EnableWebMvc
@ComponentScan("cn.gzjp.modules.*.api.web")
/**
 *
 */
public class SwaggerConfig {
	
	
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
        		.select()  // 选择那些路径和api会生成document
                .apis(RequestHandlerSelectors.any()) // 对所有api进行监控
                //.paths(PathSelectors.any()) // 对所有路径进行监控
                .paths(or(regex("/api/.*")))//过滤的接口
                .build();
    }
    private ApiInfo apiInfo() {
    	Contact contact = new Contact("zengcp","","83004003@qq.com");
    	ApiInfo apiInfo = new 
        		ApiInfo("分享社区微信小程序API接口","提供给小程序调用的接口","1.0","https://wz.starsns.cn",contact,"",null);
        return apiInfo;
    }


}