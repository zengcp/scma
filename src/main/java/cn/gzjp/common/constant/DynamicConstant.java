package cn.gzjp.common.constant;

import com.sun.tools.doclets.formats.html.SourceToHTMLConverter;

/**
 *
 */
public enum DynamicConstant {

    /**
     * 自动
     */
    AUTO(1, "auto"),

    /**
     * 手动
     */
    MAN(1, "man"),

    /**
     * 评论
     */
    COMMENT(1,"comment"),
    /**
     * 回复评论
     */
    REPLYCOMMENT(2,"replycomment"),

    /**
     * 话题
     */
    TOPIC(1,"topic"),
    /**
     * 动态
     */
    CNT(1,"cnt"),
    /**
     * 转发
     */
    FORWARD(1,"forward"),

    /**
     * 通过
     */
    PASS(0,"pass"),
    /**
     * 不通过
     */
    UNPASS(1,"unpass"),
    /**
     * 待审核
     */
    UNAUDIT(1,"unaudit");


    public int code;

    public String message;

    DynamicConstant(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static void main(String[] args) {
        System.out.println(DynamicConstant.AUTO.getMessage());
    }

}
