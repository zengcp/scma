package cn.gzjp.common.constant;

/**
 * @Author zengcp
 * @Date 2018/3/8
 */
public class DynamicOperateType {

    /**
     * 评论
     */
    public final static String COMMENT = "1";
    /**
     * 回复评论
     */
    public final static String REPLYCOMMENT = "2";

}
