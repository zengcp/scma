/**
 *  Copyright 2014 广州金鹏
 *     all right reserved
 * @author:Mr.Yellow
 *     2014年12月25日
 */
package cn.gzjp.common.utils.pay.enums;

/**
 * OrderStatus 2014年12月25日
 */
public enum OrderStatus {
	NOTPAY("未支付", 0), REVOKED("已撤销", -2), USERPAYING("用户支付中", 0), PAYERROR(
			"支付失败", -1),

	REFUND("转入退款", -1), CLOSED("已关闭", -1), ERROR("未知错误", -2), SUCCESS("支付成功", 1);

	private String value;

	private Integer option;

	public String getValue() {
		return value;
	}

	public Integer getOption() {
		return option;
	}

	private OrderStatus(String value, Integer option) {
		this.value = value;
		this.option = option;
	}

	//
}
