/**
 * $Id: PayConfig.java Nov 20, 2014 2:18:56 PM hdp
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.gzjp.common.utils.pay;


/**
 * 配置账号信息
 * <p>
 * 创建时间: Nov 20, 2014 2:18:56 PM
 * </p>
 * 
 * @author <a href="mailto:hongdanping@163.com">hdp</a>
 * @version V1.0
 * @since V1.0
 */
public final class PayConfig {

	private PayConfig() {
		throw new RuntimeException("can't init");
	}

	

	/**
	 * 本例程通过curl使用HTTP POST方法，此处可修改其超时时间，默认为30秒
	 */
	public static final int CURL_TIMEOUT = 30;
	/**
	 * 统一支付接口
	 */
	public static final String CURL_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	
	/**
	 * 查询订单接口
	 * */
	public static final String  QUERY_ORDER= "https://api.mch.weixin.qq.com/pay/orderquery";

	/**
	 * 退款接口
	 */
	public static final String REFUND_URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";
	/**
	 * 退款查询
	 */
	public static final String REFUND_QUERY_URL = "https://api.mch.weixin.qq.com/pay/refundquery";
}
