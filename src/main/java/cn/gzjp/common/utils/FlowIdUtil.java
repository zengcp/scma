package cn.gzjp.common.utils;

import java.util.Date;

/*
 * 获取以时间开头的流水号，格式：yyyyMMddHHmmssIIIIIIII 共22位
 */
public class FlowIdUtil {
	
	private static FlowIdUtil instance;
	private int seq_idx = 0 ;
	private String seq_no = "" ;
	
	
	public FlowIdUtil(){
		
	}
	
	/*
	 * 单例
	 */
	public static FlowIdUtil getInstance(){
		if(instance==null){
			synchronized (FlowIdUtil.class) {
				if(instance==null){
					instance = new FlowIdUtil();
				}
			}
		}
		return instance;
	}
	
	/*
	 * 获取以时间开头的流水号，格式：yyyyMMddHHmmssIIIIIIII 共22位
	 */
//	/*public Long getFlowid(){
//		long flowid=Long.parseLong(DateUtil.formatDate(new Date(),
//				DateUtil.DATE_FORMAT_yyyyMMddHHmmss)+getSeq_no());
//		return flowid;
//	}*/

	
	public Long getFlowidNoYear(){
		long flowid=Long.parseLong(DateUtils.formatDate(new Date(),
				"yyMMddHHmmss")+getSeq_no());
		return flowid;
	}
	public String getFlowId(){
		return DateUtils.formatDate(new Date(),"yyMMddHHmmss")+getSeq_no();
	}

	/*
	 * 自增序号，暂停8位长度
	 */
	public synchronized String getSeq_no() {
		seq_no=getSeq_idx()+"";
		while(seq_no.length()<2){
			seq_no="0"+seq_no;
		}
		return seq_no;
	}

	public void setSeq_no(String seq_no) {
		this.seq_no = seq_no;
	}

	public int getSeq_idx() {
		
		if(seq_idx >= 99){
			setSeq_idx(1);
		}else{
			setSeq_idx(++seq_idx);
		}
		return seq_idx;
	}

	public void setSeq_idx(int seq_idx) {
		this.seq_idx = seq_idx;
	}
	
	public static void main(String[] args){
		for(int i=0;i<100000;i++){
			System.out.println(FlowIdUtil.getInstance().getFlowId());
			
			
		}
	}

	
}
