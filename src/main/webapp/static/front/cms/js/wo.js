// JavaScript Document
/*动态判断手机分辨率适配
(function (doc, win) {
          var docEl = doc.documentElement,
            resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
            recalc = function () {
              var clientWidth = docEl.clientWidth;
              if (!clientWidth) return;
              docEl.style.fontSize = 16 * (clientWidth / 320) + 'px';
            };

          if (!doc.addEventListener) return;
          win.addEventListener(resizeEvt, recalc, false);
          doc.addEventListener('DOMContentLoaded', recalc, false);
        })(document, window);
*/

$(document).ready(function(){
	<!--控制字数，超出后显示省略号-->
/*	jQuery(function(){
		//使用id选择器;例如:tab对象->tr->td对象.
		$(".a_t a").each(function(i){
			//获取td当前对象的文本,如果长度大于25;
			if($(this).text().length>40){
				//给td设置title属性,并且设置td的完整值.给title属性.
				$(this).attr("title",$(this).text());
				//获取td的值,进行截取。赋值给text变量保存.
				var text=$(this).text().substring(0,40)+"...";
				//重新为td赋值;
				$(this).text(text);
			}
		});
	});*/
	/*返回*/
	$(".icon-reply,.icon-remove").click(function(){
		history.back(-1);
	});
	//通用弹窗
	$(".cancel").click(function(){
		$(".reveal-modal-bg,.pay_modal,.pay_modal2,.pay_nav,.address_list").fadeOut(300);
	});
});
/*第一种形式 第二种形式 更换显示样式*/
function setTab(name,cursel,n){
	for(i=1;i<=n;i++){
		if(cursel==i){
			$("#"+name+""+i).addClass("hover");
			$("#con_"+name+"_"+i).addClass("hover");
			$("#con_"+name+"_"+i).css('display','block');
		}else{
			$("#"+name+""+i).removeClass();
			$("#con_"+name+"_"+i).removeClass();
			$("#con_"+name+"_"+i).css('display','none');
		}
	}
}
