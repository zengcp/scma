<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>${fns:getConfig('productName')}</title>

	<%@ include file="/WEB-INF/views/include/acehead.jsp"%>
	<script src="${ctxStatic}/common/inspinia-ace.js?v=3.2.0"></script>
	<script src="${ctxStatic}/common/contabs.js"></script> 

</head>

<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">菜单</span>
		
					<span class="icon-bar"></span>
		
					<span class="icon-bar"></span>
		
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-header pull-left">
								<a href="/" class="navbar-brand" style="font-size:16px;" target="newWin">${fns:getConfig('productName')}</a><!-- /.brand -->
					<!--<a href="/apis" class="navbar-brand" style="font-size:16px;" target="newWin">API列表</a> /.brand -->
				</div><!-- /.navbar-header -->
				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav" style="">
						<li class="white">
							当前用户：<b>${fns:getUser().name}</b> &nbsp;&nbsp;&nbsp;
						</li>
		        <li class="green">
							<a  class="J_menuItem" href="${ctx }/sys/user/info">
								<i class="ace-icon fa fa-cog"></i>
								个人资料
							</a>
						</li>
						<li class="purple">
							<a class="J_menuItem" href="${ctx}/logout" onclick="return confirmx('确认要退出吗？', this.href)">
								<i class="ace-icon fa fa-power-off"></i>
								退出
							</a>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>
		


		<!-- /section:basics/navbar.layout -->
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar                  responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>



						 <t:aceMenu  menu="${fns:getTopMenu()}"></t:aceMenu>

				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<!-- /section:basics/sidebar.layout.minimize -->
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs" id="breadcrumbs">
				  <div class="content-tabs">
                <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i>
                </button>
                <nav class="page-tabs J_menuTabs">
                    <div class="page-tabs-content">
                         <a href="javascript:;" class="active J_menuTab" data-id="${ctx}/home">首页</a>
                    </div>
                </nav>
               
            	</div>
					</div>

			<div class="J_mainContent"  id="content-main">
             <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="${ctx}/home" frameborder="0" data-id="${ctx}/home" seamless></iframe>
            </div>
            </div>
            
            
            </div>
            
			
            </div>
            
				

	</body>
	<script type="text/javascript">
	
	function changeStyle(){
		   $.get('${pageContext.request.contextPath}/theme/default?url='+window.top.location.href,function(result){ window.location.reload();  });
		}
	</script>

</html>