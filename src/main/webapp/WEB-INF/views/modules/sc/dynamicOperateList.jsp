<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>评论管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>评论列表 </h5>
	</div>
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="dynamicOperate" action="${ctx}/sc/dynamicOperate/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>内容：</span>
			<form:input path="content" htmlEscape="false"  class=" form-control input-sm"/>
			<span>状态：</span>
				<form:select path="status"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('content_status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			<div class="pull-right">
				<button  class="btn btn-primary btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			</div>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<shiro:hasPermission name="sc:dynamicOperate:add">
				<table:addRow url="${ctx}/sc/dynamicOperate/form" title="回复评论"></table:addRow>
			</shiro:hasPermission>
			<shiro:hasPermission name="sc:dynamicOperate:edit">
			    <table:editRow url="${ctx}/sc/dynamicOperate/form" title="回复评论" id="contentTable"></table:editRow>
			</shiro:hasPermission>
			<shiro:hasPermission name="sc:dynamicOperate:del">
				<table:delRow url="${ctx}/sc/dynamicOperate/deleteAll" id="contentTable"></table:delRow>
			</shiro:hasPermission>
			<shiro:hasPermission name="sc:dynamicOperate:import">
				<table:importExcel url="${ctx}/sc/dynamicOperate/import"></table:importExcel>
			</shiro:hasPermission>
			<shiro:hasPermission name="sc:dynamicOperate:export">
	       		<table:exportExcel url="${ctx}/sc/dynamicOperate/export"></table:exportExcel>
	       	</shiro:hasPermission>
	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		
	</div>
	</div>
	-->
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th  class="sort-column content">内容</th>
				<th  class="sort-column memberId">会员</th>
				<th  class="sort-column dynamicId">动态</th>
				<th  class="sort-column createDate">回复时间</th>
				<th  class="sort-column status">状态</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="dynamicOperate">
			<tr>

				<td>
					<a href="#" onclick="openDialogView('查看评论', '${ctx}/sc/dynamicOperate/reply?id=${dynamicOperate.id}','800px', '500px')"  >
						${fns:abbr(dynamicOperate.content,48)}
					</a>
				</td>
				<td>
					<a href="#" onclick="openDialogView('查看会员', '${ctx}/sc/member/form?id=${dynamicOperate.memberId}','600px', '360px')">
					${dynamicOperate.memberName}
					</a>
				</td>
				<td>
					<a href="#" onclick="openDialogView('查看动态', '${ctx}/sc/dynamic/form?id=${dynamicOperate.dynamicId}','800px', '500px')">
							${fns:abbr(dynamicOperate.dynamicContent,32)}
					</a>
				</td>

				<td>
					<fmt:formatDate value="${dynamicOperate.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${fns:getDictLabel(dynamicOperate.status, 'content_status', '')}
				</td>
				<td>

					<shiro:hasPermission name="sc:dynamicOperate:edit">
    					<a href="#" onclick="openDialog('回复评论', '${ctx}/sc/dynamicOperate/reply?id=${dynamicOperate.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 回复</a>
    				</shiro:hasPermission>
    				<shiro:hasPermission name="sc:dynamicOperate:del">
						<a href="${ctx}/sc/dynamicOperate/delete?id=${dynamicOperate.id}" onclick="return confirmx('确认要删除该评论吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
					</shiro:hasPermission>
					<c:if test="${dynamicOperate.status eq '1'}">
						<a href="${ctx}/sc/dynamicOperate/audit?id=${dynamicOperate.id}&status=0" onclick="return confirmx('确认要通过该内容吗？', this.href)"   class="btn btn-primary btn-xs">通过</a>
					</c:if>
					<c:if test="${dynamicOperate.status eq '0'}">
						<a href="${ctx}/sc/dynamicOperate/audit?id=${dynamicOperate.id}&status=1" onclick="return confirmx('确认要不通过该内容吗？', this.href)"   class="btn btn-danger btn-xs">不通过</a>
					</c:if>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>