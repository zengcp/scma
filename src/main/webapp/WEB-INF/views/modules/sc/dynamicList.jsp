<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>动态信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>动态信息列表 </h5>
	</div>
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
		<form:form id="searchForm" modelAttribute="dynamic" action="${ctx}/sc/dynamic/" method="post" class="form-inline">
			<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
			<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
			<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
			<div class="form-group">
				<span>内容：</span>
				<form:input path="content" htmlEscape="false"  class=" form-control input-sm"/>
				<span>状态：</span>
				<form:select path="status"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('content_status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span>审核方式：</span>
				<form:select path="auditWay"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('audit_way')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<div class="pull-right">
					<button  class="btn btn-primary btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
				</div>
			</div>
		</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<shiro:hasPermission name="sc:dynamic:add">
				<table:addRow url="${ctx}/sc/dynamic/form" title="动态信息"></table:addRow>
			</shiro:hasPermission>
			<shiro:hasPermission name="sc:dynamic:edit">
			    <table:editRow url="${ctx}/sc/dynamic/form" title="动态信息" id="contentTable"></table:editRow>
			</shiro:hasPermission>
			<shiro:hasPermission name="sc:dynamic:del">
				<table:delRow url="${ctx}/sc/dynamic/deleteAll" id="contentTable"></table:delRow>
			</shiro:hasPermission>

	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		
	</div>
	</div>
	-->
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th  class="sort-column content">内容</th>
				<th  class="sort-column memberId">会员</th>
				<th  class="sort-column commentCount">评论量</th>
				<th  class="sort-column forwardCount">转发量</th>
				<th  class="sort-column collectionCount">收藏量</th>
				<th  class="sort-column type">类型</th>
				<th  class="sort-column originalId">原创</th>
				<th  class="sort-column status">状态</th>
				<th  class="sort-column auditWay">审核方式</th>
				<th  class="sort-column createDate">创建时间</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="dynamic">
			<tr>
				<td>
							<a href="#" onclick="openDialogView('查看动态详情', '${ctx}/sc/dynamic/form?id=${dynamic.id}','800px', '500px')"  >
									${fns:abbr(dynamic.content,48)}>>
							</a>
				</td>
				<td>
					<a href="#" onclick="openDialogView('查看会员', '${ctx}/sc/member/form?id=${dynamic.memberId}','600px', '360px')">
					${dynamic.memberName}
					</a>
				</td>
				<td>
					<a href="#" onclick="openDialogView('查看评论', '${ctx}/sc/dynamicOperate/list?dynamicId=${dynamic.id}','800px', '500px')">
							${dynamic.commentCount}
					</a>
				</td>
				<td>
					<a href="#" onclick="openDialogView('查看转发', '${ctx}/sc/dynamic/list?originalId=${dynamic.id}','800px', '500px')">
							${dynamic.forwardCount}
					</a>
				</td>
				<td>
					${dynamic.collectionCount}
				</td>
				<td>
					${fns:getDictLabel(dynamic.type, 'content_type', '')}
				</td>
				<td>
						<c:if test="${not empty dynamic.originalId}">
						<a href="#" onclick="openDialogView('查看原创详情', '${ctx}/sc/dynamic/form?id=${dynamic.originalId}','800px', '500px')"  >
							查看原创
						</a>
						</c:if>
				</td>
				<td>

						<c:if test="${dynamic.status eq '0' }"><span class="btn btn-primary btn-rounded btn-xs"></c:if>
						<c:if test="${dynamic.status eq '1' }"><span class="btn btn-danger btn-rounded btn-xs"></c:if>
							<c:if test="${dynamic.status eq '2' }"><span class="btn btn-danger btn-rounded btn-xs"></c:if>
					${fns:getDictLabel(dynamic.status, 'content_status', '')}
							</span>


				</td>
				<td>
					${fns:getDictLabel(dynamic.auditWay, 'audit_way', '')}
				</td>
				<td>
					<fmt:formatDate value="${dynamic.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<!--
					<shiro:hasPermission name="sc:dynamic:view">
						<a href="#" onclick="openDialogView('查看动态信息', '${ctx}/sc/dynamic/form?id=${dynamic.id}','800px', '500px')" class="btn btn-info btn-xs" ><i class="fa fa-search-plus"></i> 查看</a>
					</shiro:hasPermission>

    				<shiro:hasPermission name="sc:dynamic:del">
						<a href="${ctx}/sc/dynamic/delete?id=${dynamic.id}" onclick="return confirmx('确认要删除该动态信息吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
					</shiro:hasPermission>
-->                 <c:if test="${dynamic.status eq '1'||dynamic.status eq '2'}">
					<a href="${ctx}/sc/dynamic/audit?id=${dynamic.id}&status=0" onclick="return confirmx('确认要通过该内容吗？', this.href)"   class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>通过</a>
				    </c:if>
					<c:if test="${dynamic.status eq '0'||dynamic.status eq '2'}">
					 <a href="${ctx}/sc/dynamic/audit?id=${dynamic.id}&status=1" onclick="return confirmx('确认要不通过该内容吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-edit"></i>不通过</a>
					</c:if>

						<a href="${ctx}/sc/dynamic/delete?id=${dynamic.id}" onclick="return confirmx('确认要删除该吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>