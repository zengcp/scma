<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>会员管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>会员列表 </h5>
	</div>
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="member" action="${ctx}/sc/member/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>编号：</span>
				<form:input path="id" htmlEscape="false" maxlength="32"  class=" form-control input-sm"/>
			<span>昵称：</span>
				<form:input path="nickName" htmlEscape="false" maxlength="128"  class=" form-control input-sm"/>
			<div class="pull-right">
				<button  class="btn btn-primary btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			</div>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	

	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th  class="sort-column id">编号</th>
				<th  class="sort-column nickName">昵称</th>
				<th  class="sort-column avatarUrl">头像</th>
				<!--<th  class="sort-column gender">性别</th>
				<th  class="sort-column mobile">手机号</th>-->
				<th  class="sort-column dynamicCount">动态</th>
				<th  class="sort-column followCount">关注</th>
				<th  class="sort-column fansCount">粉丝</th>
				<th  class="sort-column createDate">注册时间</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="member">
			<tr>
				<td>
					${member.id}
				</td>
				<td>
					${member.nickName}
				</td>
				<td>
					<img src="${member.avatarUrl}" width="90px">
				</td>
				<!--
				<td>
					${member.gender}
				</td>

				<td>
					${member.mobile}
				</td>-->
				<td>

						<a href="#" onclick="openDialogView('动态列表', '${ctx}/sc/dynamic/list?memberId=${member.id}','800px', '500px')">
								${member.dynamicCount}
						</a>
				</td>
				<td>
						${member.followCount}
				</td>
				<td>
						${member.fansCount}
				</td>

				<td>
					<fmt:formatDate value="${member.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<c:if test="${member.id eq '666'}">
    					<a href="#" onclick="openDialog('修改官方头像与昵称', '${ctx}/sc/member/form?id=${member.id}','520px', '400px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
					</c:if>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>