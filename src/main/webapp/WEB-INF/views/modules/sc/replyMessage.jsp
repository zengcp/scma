<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>回复消息</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});
	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="message" action="${ctx}/sc/officialMessage/reply" method="post" class="form-horizontal">
		<form:hidden path="id"/>
			<sys:message content="${message2}"/>
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
			   <tr>

				   <td class="width-15 active"><label class="pull-right">消息内容：</label></td>
				   <td class="width-35">
					   ${message.content}
				   </td>
			   </tr>
				<tr>

					<td class="width-15 active"><label class="pull-right">回复内容：</label></td>
					<td class="width-35">
						<form:textarea path="replyContent" htmlEscape="false" rows="6"    class="form-control required"/>
					</td>
				</tr>

		 	</tbody>
		</table>
	</form:form>
</body>
</html>