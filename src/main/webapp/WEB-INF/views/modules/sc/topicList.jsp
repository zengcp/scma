<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>动态信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>话题列表 </h5>
	</div>
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
		<form:form id="searchForm" modelAttribute="dynamic" action="${ctx}/sc/topic/" method="post" class="form-inline">
			<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
			<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
			<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
			<div class="form-group">
				<span>名称：</span>
				<form:input path="topicNames" htmlEscape="false"  class=" form-control input-sm"/>
				<div class="pull-right">
					<button  class="btn btn-primary btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
				</div>
			</div>
		</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<shiro:hasPermission name="sc:dynamic:add">
				<table:addRow url="${ctx}/sc/dynamic/form" title="动态信息"></table:addRow>
			</shiro:hasPermission>
			<shiro:hasPermission name="sc:dynamic:edit">
			    <table:editRow url="${ctx}/sc/dynamic/form" title="动态信息" id="contentTable"></table:editRow>
			</shiro:hasPermission>
			<shiro:hasPermission name="sc:dynamic:del">
				<table:delRow url="${ctx}/sc/dynamic/deleteAll" id="contentTable"></table:delRow>
			</shiro:hasPermission>

	       <button class="btn btn-white btn-sm " data-toggle="tooltip" data-placement="left" onclick="sortOrRefresh()" title="刷新"><i class="glyphicon glyphicon-repeat"></i> 刷新</button>
		
			</div>
		
	</div>
	</div>
	-->
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th  class="sort-column name">话题名称</th>

				<th  class="sort-column memberId">创建者</th>
				<th  class="sort-column commentCount">动态数量</th>
				<th  class="sort-column createDate">创建时间</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="dynamic">
			<tr>
				<td>
							<a href="#" onclick="openDialogView('查看话题详情', '${ctx}/sc/topic/form?id=${dynamic.id}','600px', '350px')"  >
									${dynamic.topicNames}
							</a>
				</td>
				<td>
					<a href="#" onclick="openDialogView('查看会员', '${ctx}/sc/member/form?id=${dynamic.memberId}','600px', '360px')">
					${dynamic.memberName}
					</a>
				</td>
				<td>
					${dynamic.commentCount}
				</td>
				<td>
					<fmt:formatDate value="${dynamic.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>

    				<shiro:hasPermission name="sc:dynamic:del">
						<a href="${ctx}/sc/topic/delete?id=${dynamic.id}" onclick="return confirmx('确认要删除该话题吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
					</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>