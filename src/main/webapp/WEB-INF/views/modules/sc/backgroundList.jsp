<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>背景信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>背景信息 </h5>
	</div>
    <div class="ibox-content">
	<sys:message content="${message}"/>
	


	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th  class="sort-column color">背景颜色</th>
				<th  class="sort-column pic">背景图</th>
				<th  class="sort-column updateDate">更新时间</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="background">
			<tr>
				<td>
					${background.color}
				</td>
				<td>
						<img alt="" src="${background.picUrl}" width="320px" >
				</td>
				<td>
					<fmt:formatDate value="${background.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<shiro:hasPermission name="sc:background:view">
						<a href="#" onclick="openDialogView('查看背景信息', '${ctx}/sc/background/form?id=${background.id}','800px', '500px')" class="btn btn-info btn-xs" ><i class="fa fa-search-plus"></i> 查看</a>
					</shiro:hasPermission>
					<shiro:hasPermission name="sc:background:edit">
    					<a href="#" onclick="openDialog('修改背景信息', '${ctx}/sc/background/form?id=${background.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
    				</shiro:hasPermission>
    				<shiro:hasPermission name="sc:background:del">
						<a href="${ctx}/sc/background/delete?id=${background.id}" onclick="return confirmx('确认要删除该背景信息吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
					</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>