<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>消息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>消息列表 </h5>
	</div>
    <div class="ibox-content">
		<sys:message content="${message2}"/>
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="message" action="${ctx}/sc/officialMessage/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>内容：</span>
				<form:input path="content" htmlEscape="false"  class=" form-control input-sm"/>
			<div class="pull-right">
				<button  class="btn btn-primary btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			</div>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	

	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th  class="sort-column content">内容</th>
				<th  class="sort-column senderId">消息发送者</th>
				<th  class="sort-column createDate">创建时间</th>
				<!--<th  class="sort-column status">状态</th>-->
				<th  class="sort-column replyContent">官方回复</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="message">
			<tr>
				<td>
					${message.content}
				</td>
				<td>
					${message.senderName}
				</td>

				<td>
					<fmt:formatDate value="${message.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<!--
				<td>
					${message.status}
				</td>
				-->
				<td>
					${message.replyContent}
				</td>
				<td>

					<shiro:hasPermission name="sc:message:edit">
						<a href="#" onclick="openDialog('回复', '${ctx}/sc/officialMessage/reply?id=${message.id}','800px', '500px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 回复</a>
					</shiro:hasPermission>
    				<shiro:hasPermission name="sc:message:del">
						<a href="${ctx}/sc/officialMessage/delete?id=${message.id}" onclick="return confirmx('确认要删除该消息吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
					</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
</body>
</html>